# Prilis nazorna implementacia
def ea(ma, mi):
    assert ma > mi
    d = ma // mi
    r = ma % mi
    print(f'{ma} = {mi} * {d} + {r}')
    if r == 0:
        return (r, ma, mi)
    return ea(mi, r)

# Pomerne normalna implementacia
def eaa(a, b):
    assert a > b
    a %= b
    if a == 0:
        return b
    return eaa(b, a)

# Rozumne kompaktna implementacia
def compact(a, b):
    assert a > b
    a %= b
    return b if a == 0 else compact(b, a)

# Nerozumne kompaktna implementacia
c=lambda a,b:b if a%b==0 else c(b,a%b)

# Cykly
def eacycles(a, b):
    assert a > b
    while True:
        a %= b
        if a == 0:
            return b
        a, b = b, a
