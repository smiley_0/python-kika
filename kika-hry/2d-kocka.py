import pygame, sys
pygame.init()
pygame.font.init()

size = width, height = 800, 600
black = 0, 0, 0

pismo = pygame.font.SysFont('Comic Sans MS', 42)
textsurface = pismo.render('42', False, (255, 255, 255))
koniec = pismo.render('koniec', False, (255, 255, 255))

zivoty = 3

screen = pygame.display.set_mode(size)

def trojuholnik_teda_zivot(x, y):
    pygame.draw.polygon(screen, (176,224,230), [
        (x, y),
        (x+20, y),
        (x+10, y-10),
    ])

# left, top, width, height
obal = pygame.Rect(2, 86, 42, 42)
kruh = pygame.Rect(44, 450, 42, 42)
ciarky = [
    pygame.Rect(86, 0, 2, 420),
    pygame.Rect(240, 200, 2, 400), # dvojstena
    pygame.Rect(240, 0, 2, 100),
]
steny = [
    # velke steny
    pygame.Rect(44, 0, 42, 420),
    pygame.Rect(242, 200, 42, 400), #vdvojstena
    pygame.Rect(242, 0, 42, 100),
    pygame.Rect(642, 200, 42, 400),
    # ohradka
    pygame.Rect(0, 0, 2, 600),
    pygame.Rect(0, 0, 800, 2),
    pygame.Rect(798, 0, 2, 600),
    pygame.Rect(0, 598, 800, 2),
]
male_steny = [
    pygame.Rect(86, 340, 75, 42),
    pygame.Rect(190, 460, 55, 42), # prava
    pygame.Rect(190, 220, 55, 42), # prava
    pygame.Rect(86, 100, 75, 42),
]
nepriatelia = [
    pygame.Rect(380, -221, 42, 21),
    pygame.Rect(480, -63, 42, 21),
    pygame.Rect(530, 42, 42, 21),
    pygame.Rect(580, -113, 42, 21),
]
vyherne_policko = pygame.Rect(650, 42, 100, 80)

# zaciatok zaujimavych veci
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_r:
                kruh = pygame.Rect(44, 450, 42, 42)

    aa = pygame.key.get_pressed()
    TOP = kruh.top
    LEFT = kruh.left

    if zivoty > 0:
        if aa[pygame.K_UP]:
            kruh.top -= 1
        if aa[pygame.K_DOWN]:
            kruh.top += 1
        if aa[pygame.K_LEFT]:
            kruh.left -= 1
        if aa[pygame.K_RIGHT]:
            kruh.left += 1

    for stena in steny:
        if kruh.colliderect(stena):
            kruh.top = TOP
            kruh.left = LEFT

    if kruh.colliderect(obal):
        kruh = pygame.Rect(44, 450, 42, 42)
        zivoty -= 1
        obal.top -= 42

    for nepriatel in nepriatelia:
        nepriatel.top += 1
        if nepriatel.top >= 600:
            nepriatel.top = -21
        if kruh.colliderect(nepriatel):
            kruh = pygame.Rect(44, 450, 42, 42)
            zivoty -= 1

    for mala_stena in male_steny:
        if kruh.colliderect(mala_stena):
            kruh = pygame.Rect(44, 450, 42, 42)
            zivoty -= 1

    for ciarka in ciarky:
        if kruh.colliderect(ciarka):
            kruh = pygame.Rect(44, 450, 42, 42)
            zivoty -= 1

    # hranica
    screen.fill(black)

    if zivoty <= 0:
        screen.blit(koniec, (300,400))

    for i in range(zivoty):
        trojuholnik_teda_zivot(10, i*30+20)

    for mala_stena in male_steny:
        pygame.draw.rect(screen, (25,25,112), mala_stena)

    if vyherne_policko.contains(kruh):
        screen.blit(textsurface,(300,400))

    for nepriatel in nepriatelia:
        pygame.draw.rect(screen, (25,25,112), nepriatel)

    for stena in steny:
        pygame.draw.rect(screen, (1, 42, 47), stena)

    for ciarka in ciarky:
        pygame.draw.rect(screen, (25,25,112), ciarka)

    pygame.draw.rect(screen, (0, 255, 0), vyherne_policko)
    pygame.draw.rect(screen, (255, 0, 0), kruh)

    pygame.display.flip()