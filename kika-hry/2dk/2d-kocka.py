import importlib
import sys

import level1 as level
import pygame

pygame.init()
pygame.font.init()

size = width, height = 800, 600
black = 0, 0, 0

pismo = pygame.font.SysFont('Comic Sans MS', 42)
textsurface = pismo.render('42', False, (255, 255, 255))
koniec = pismo.render('koniec', False, (255, 255, 255))

zivoty = 3
obal = pygame.Rect(2, 86, 42, 42)

kruh = pygame.Rect(44, 450, 42, 42)
#kruh = pygame.Rect(650, 42, 42, 42)

screen = pygame.display.set_mode(size)

def trojuholnik_teda_zivot(x, y):
    pygame.draw.polygon(screen, (176,224,230), [
        (x, y),
        (x+20, y),
        (x+10, y-10),
    ])


# zaciatok zaujimavych veci
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_r:
                kruh = pygame.Rect(44, 450, 42, 42)

    aa = pygame.key.get_pressed()
    TOP = kruh.top
    LEFT = kruh.left

    if zivoty > 0:
        if aa[pygame.K_UP]:
            kruh.top -= 1
        if aa[pygame.K_DOWN]:
            kruh.top += 1
        if aa[pygame.K_LEFT]:
            kruh.left -= 1
        if aa[pygame.K_RIGHT]:
            kruh.left += 1

    for stena in level.steny:
        if kruh.colliderect(stena):
            kruh.top = TOP
            kruh.left = LEFT

    if kruh.colliderect(obal):
        kruh.left, kruh.top = level.suradnice_kruhu
        zivoty -= 1
        obal.top -= 42

    for nepriatel in level.nepriatelia:
        nepriatel.top += 1
        if nepriatel.top >= 600:
            nepriatel.top = -21
        if kruh.colliderect(nepriatel):
            kruh.left, kruh.top = level.suradnice_kruhu
            zivoty -= 1

    for vodorovnak in level.nepriatelia_vodorovne:
        vodorovnak.left += level.smer
        if vodorovnak.left <= level.max_n_v:
            vodorovnak.left = level.posun_n_v
        if kruh.colliderect(vodorovnak):
            kruh.left, kruh.top = level.suradnice_kruhu
            zivoty -= 1

    # toto mi moc nefunguje
    for mostik in level.most:
        if level.mostorychlost == 5:
            if mostik.left == 300 or mostik.left == 186:
                level.mostosmer *= -1
                if level.wait == 5:
                    mostik.left += level.mostosmer
                level.wait -= 1

            if level.wait == 0:
                level.wait = 5
        level.mostorychlost -= 1

        if level.mostorychlost == 0:
            level.mostorychlost = 5


        for mala_stena in level.male_steny:
            if kruh.colliderect(mala_stena):
                if kruh.colliderect(mostik):
                    pass
                else:
                    kruh.left, kruh.top = level.suradnice_kruhu
                    zivoty -= 1

    for ciarka in level.ciarky:
        if kruh.colliderect(ciarka):
            kruh.left, kruh.top = level.suradnice_kruhu
            zivoty -= 1

    # hranica
    screen.fill(black)

    if zivoty <= 0:
        screen.blit(koniec, (300,400))

    for i in range(zivoty):
        trojuholnik_teda_zivot(10, i*30+20)

    for mala_stena in level.male_steny:
        pygame.draw.rect(screen, (25,25,112), mala_stena)

    if level.vyherne_policko.contains(kruh):
        screen.blit(textsurface,(300,400))
        if level.NEXT_LEVEL is not None:
            level = importlib.import_module(level.NEXT_LEVEL)

    for nepriatel in level.nepriatelia:
        pygame.draw.rect(screen, (25,25,112), nepriatel)

    for vodorovnak in level.nepriatelia_vodorovne:
        pygame.draw.rect(screen, (25,25,112), vodorovnak)

    for stena in level.steny:
        pygame.draw.rect(screen, (1, 42, 47), stena)

    for ciarka in level.ciarky:
        pygame.draw.rect(screen, (25,25,112), ciarka)

    for mostik in level.most:
        pygame.draw.rect(screen, (1, 42, 47), mostik )

    pygame.draw.rect(screen, (0, 255, 0), level.vyherne_policko)
    pygame.draw.rect(screen, (255, 0, 0), kruh)

    pygame.display.flip()
