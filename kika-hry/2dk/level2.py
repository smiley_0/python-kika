import pygame
NEXT_LEVEL = None

# left, top, width, height

suradnice_kruhu = (650, 42)

ciarky = []

steny = [
    # velke steny
    pygame.Rect(500, 0, 42, 480),
    # ohradka
    pygame.Rect(0, 0, 2, 600),
    pygame.Rect(0, 0, 800, 2),
    pygame.Rect(798, 0, 2, 600),
    pygame.Rect(0, 598, 800, 2),
]

male_steny = [
    pygame.Rect(186, 0, 214, 600)
]

nepriatelia = []

nepriatelia_vodorovne = [
    pygame.Rect(880, 110, 42, 10),
    pygame.Rect(680, 220, 42, 10),
    pygame.Rect(730, 330, 42, 10),
    pygame.Rect(780, 440, 42, 10),
]

# kam az moze ist nepriatel,
# kam ho to hodi
smer = -1
max_n_v = 542
posun_n_v = 842

most = [
    pygame.Rect(300, 84, 100, 80)
]

mostosmer = 1
mostorychlost = 5
wait = 5

vyherne_policko = pygame.Rect(44, 450, 100, 80)
