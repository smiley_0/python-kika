import pygame
NEXT_LEVEL = 'level2'

suradnice_kruhu = (440, 45)

# left, top, width, height
obal = pygame.Rect(2, 86, 42, 42)
kruh = pygame.Rect(440, 45, 42, 42)

ciarky = [
    pygame.Rect(86, 0, 2, 420),
    pygame.Rect(240, 200, 2, 400), # dvojstena
    pygame.Rect(240, 0, 2, 100),
]

steny = [
    # velke steny
    pygame.Rect(44, 0, 42, 420),
    pygame.Rect(242, 200, 42, 400), #vdvojstena
    pygame.Rect(242, 0, 42, 100),
    pygame.Rect(642, 200, 42, 400),
    # ohradka
    pygame.Rect(0, 0, 2, 600),
    pygame.Rect(0, 0, 800, 2),
    pygame.Rect(798, 0, 2, 600),
    pygame.Rect(0, 598, 800, 2),
]

male_steny = [
    pygame.Rect(86, 340, 75, 42),
    pygame.Rect(190, 460, 55, 42), # prava
    pygame.Rect(190, 220, 55, 42), # prava
    pygame.Rect(86, 100, 75, 42),
]

nepriatelia = [
    pygame.Rect(380, -221, 42, 21),
    pygame.Rect(480, -63, 42, 21),
    pygame.Rect(530, 42, 42, 21),
    pygame.Rect(580, -113, 42, 21),
]

nepriatelia_vodorovne = []
most = []

vyherne_policko = pygame.Rect(650, 42, 100, 80)
