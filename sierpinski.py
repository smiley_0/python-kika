#!/usr/bin/env python3
import turtle

def triangle(x, y, width, height):
    turtle.penup()
    turtle.goto(x, y)
    turtle.pendown()
    turtle.begin_fill()
    turtle.goto(x + width, y)
    turtle.goto(x + (width / 2), y + height)
    turtle.goto(x, y)
    turtle.end_fill()

def sierpinski(x, y, iteration, width=100, height=75):
    if iteration == 0:
        triangle(x, y, width, height)
    else:
        sierpinski(x, y, iteration-1, width/2, height/2)
        sierpinski(x + (width / 2), y, iteration-1, width/2, height/2)
        sierpinski(x + (width / 4), y + (height / 2), iteration-1, width/2, height/2)

if __name__ == '__main__':
    w, h = 400, 350
    sierpinski(-w/2, -h/2, 4, w, h)
    turtle.ht()
    turtle.done()
