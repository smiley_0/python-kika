import fac

cnt = 0
for i in range(1, 1000000):
    bad = False
    mx = fac.factorMultiplicity(i)
    for f, e in mx:
        if e != 1:
            bad = True
            break
        if f >= 1000:
            bad = True
            break

    if not bad:
        print(i, mx)
        cnt += 1


print(cnt)
