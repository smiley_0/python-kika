def d(n):
    p = 0
    for i in range(1, n+1):
        if n%i == 0:
            p += 1

    return p

count = 0
for i in range(1, 5*10**7):
    count += d(i)

    if i % 1000 == 0:
        print(count)

print(count)
