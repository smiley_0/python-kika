def d(n):
    count = 2  # accounts for 'n' and '1'
    i = 2
    while i ** 2 < n:
        if n % i == 0:
            count += 2
        i += 1
    if i ** 2 == n:
        count += 1
    return count


count = 0
for i in range(1, 5*10**7):
    count += d(i)

    if i % 1000 == 0:
        print(count)

print(count)
