def s(n):
   r = 0
   while n:
       r, n = r + n % 10, n // 10
   return r

i = 0
kolke = 0
while True:
    if s(i) == 10:
        kolke += 1
        if kolke == 10001:
            break
    i += 1

print(i)
