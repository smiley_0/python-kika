#from sympy.ntheory import factorint
import fac
def d(x):
    s = 1
    for i, e in fac.factorMultiplicity(x):
        s *= e+1
    return s


count = 0
for i in range(1, 5*10**7):
    count += d(i)

    if i % 1000 == 0:
        print(count)

print(count)
