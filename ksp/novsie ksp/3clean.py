r,c = [int(i) for i in input().split()]
stan = [[-int(i) for i in input().split()] for j in range(r)]
navstivene = [[None for i in range(c)] for j in range(r)]
navstivene[0][0] = 0
pred = [[None for i in range(c)] for j in range(r)]

def pohyb(x, y):
    if y < r-1 and pred[y+1][x] != (y, x):
        yield (y+1, x)
    if x < c-1:
        yield (y, x+1)
    if c-1 > x > 0 and y > 0 and pred[y-1][x] != (y, x):
        yield (y-1, x)

for _ in range(r*c - 1):
    for y in range(r):
        for x in range(c):
            for yy, xx in pohyb(x, y):
                new_cost = navstivene[y][x] + stan[yy][xx]
                if navstivene[yy][xx] is None or navstivene[yy][xx] > new_cost:
                    navstivene[yy][xx] = new_cost
                    pred[yy][xx] = (y, x)

print(-navstivene[-1][-1])
