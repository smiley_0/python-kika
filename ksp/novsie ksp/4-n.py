n = int(input())
neviem = input()
parita = 0
diksneri = {parita:0}

najdlsi_podretazec = -1

for index, znak in enumerate(neviem):
    index+=1
    poradie = ord(znak) - ord('a')
    parita ^= 1 << poradie

    # Zapiseme novu paritu
    if parita not in diksneri:
        diksneri[parita] = index

    # Vyhladame najlavsiu
    najlavsia = n+1
    najlavsia = min(diksneri.get(parita, n+1), najlavsia)
    for i in range(20):
        mod_parita = parita ^ (1 << i)
        najlavsia = min(diksneri.get(mod_parita, n+1), najlavsia)
    najdlsi_podretazec = max(index - najlavsia, najdlsi_podretazec)


print(najdlsi_podretazec)
