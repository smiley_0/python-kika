import sys

sys.setrecursionlimit(1000000)

r,c = [int(i) for i in input().split()]
stan = [[int(i) for i in input().split()] for j in range(r)]
p = {}

def f(riadok, stlpec, odkial):
    # print(riadok, stlpec, odkial)
    if riadok == r - 1 and stlpec == c - 1:
        # print(f'In f({riadok}, {stlpec}, {odkial}) returning 0')
        return 0
    elif (riadok, stlpec, odkial) in p:
        # print(f'In f({riadok}, {stlpec}, {odkial}) returning cached {p[(riadok, stlpec, odkial)]}')
        return p[(riadok, stlpec, odkial)]
    else:
        max_value = -10000
        if odkial != 1 and riadok-1 >= 0:
            max_value = max(max_value, f(riadok-1, stlpec, 3) + stan[riadok][stlpec])
            # print(f'In f({riadok}, {stlpec}, {odkial}) New max {max_value}')
        if odkial != 3 and riadok+1 < r:
            max_value = max(max_value, f(riadok+1, stlpec, 1) + stan[riadok][stlpec])
            # print(f'In f({riadok}, {stlpec}, {odkial}) New max {max_value}')
        if stlpec+1 < c:
            max_value = max(max_value, f(riadok, stlpec+1, 2) + stan[riadok][stlpec])
            # print(f'In f({riadok}, {stlpec}, {odkial}) New max {max_value}')

        # print(f'In f({riadok}, {stlpec}, {odkial}) returning {max_value}')
        p[(riadok, stlpec, odkial)] = max_value
        return max_value

m = max(r, c)
for i in range(2, m):
    # print(f'calc {max(0, r-i)}, {max(c-i, 0)}')
    f(max(0, r-i), max(c-i, 0), -1)
print(f(0, 0, -1))
