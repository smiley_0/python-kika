def do(n):
    i = n
    while True:
        s = sum([int(x) for x in str(i)])
        if s == n:
            return i
        i += n

for i in range(100):
    print(f'{i};{do(i)}')
