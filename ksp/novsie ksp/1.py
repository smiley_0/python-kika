n = int(input())
cool = [int(i) for i in input().split()]
p = cool[::-1]
prefix = [0]

for i in range(n):
    prefix.append(prefix[i] + p[i])

print(max(prefix[1:]))
