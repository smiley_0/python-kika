from copy import deepcopy

r,c = [int(i) for i in input().split()]
stan = [[-int(i) for i in input().split()] for j in range(r)]
navstivene = [[None for i in range(c)] for j in range(r)]
navstivene[0][0] = 0
pred = [[None for i in range(c)] for j in range(r)]

#for row in stan:
#    print(row)

def je_v_ceste(start, novy):
    ny, nx = novy
    sy, sx = start

    while True:
        if sx == nx and sy == ny:
            return True
        if pred[sy][sx] is None:
            break
        sy, sx = pred[sy][sx]
    return False

def pohyb(x, y, up):
    if up and c-1 > x > 0 and y > 0 and not je_v_ceste((y, x), (y-1, x)):
        yield (y-1, x)
    if y < r-1 and not je_v_ceste((y, x), (y+1, x)):
        yield (y+1, x)
    if x < c-1:
        yield (y, x+1)

for y in range(r-1):
    navstivene[y+1][0] = navstivene[y][0] + stan[y+1][0]
ncopy = deepcopy(navstivene)
for y in range(r):
    pred = [[None for i in range(c)] for j in range(r)]
    navstivene = deepcopy(ncopy)

    for _ in range(r*c - 1):
        for y in range(r):
            for x in range(c):
                for yy, xx in pohyb(x, y, True):
                    new_cost = navstivene[y][x] + stan[yy][xx]
                    if navstivene[yy][xx] is None or navstivene[yy][xx] >= new_cost:
                        #print(y, x, '->', yy, xx, f'({new_cost}) better than {navstivene[yy][xx]}')
                        navstivene[yy][xx] = new_cost
                        pred[yy][xx] = (y, x)
                        #for row in navstivene:
                        #    print(row)
                        #print('---')
                        #for row in pred:
                        #    print(row)
                        #input()

    print(-navstivene[-1][-1])
