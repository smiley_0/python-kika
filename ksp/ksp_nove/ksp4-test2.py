DIR_1 = {'S', 'V', 'J', 'Z'}
DIR_2 = {'SV', 'JV', 'JZ', 'SZ'}

a, b = input('Initial> ').split()

def get_order(x):
    if x in DIR_1:
        return (x, 1)
    elif x in DIR_2:
        return (x, 2)

def join(a, b):
    a, ao = a
    b, bo = b

    if ao < bo:
        return (f'{a}{b}', bo+1)
    else:
        return (f'{b}{a}', ao+1)

a = get_order(a)
b = get_order(b)

while True:
    print(a, '|', b)
    iii = input('L/R/D? ')
    if iii == '1':
        a = join(a, b)
    elif iii == '0':
        b = join(a, b)
    else:
        print(join(a, b))
        break
