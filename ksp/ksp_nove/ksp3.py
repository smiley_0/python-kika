n, m, f = [int(i) for i in input().split()]
tuba = [-42] + [int(i) for i in input().split()] + [-42]
poziadavky = [0 for i in range(f + 1)]

for i in range(m):
    a, farba = [int(i) for i in input().split()]
    poziadavky[farba] = a

l = 0
r = n  # viem preco to robim ale nenapisem
dokopy = sum(poziadavky)
kopia = poziadavky[:]
splnene = [0 for x in range(f + 1)]

while dokopy > 0:
    if kopia[tuba[r]] > 0:
        kopia[tuba[r]] -= 1
        dokopy -= 1
    r -= 1

r += 1

minimum = n - r + 1
#print(l, r, minimum)

l += 1
splnene[tuba[l]] += 1

while l < n + 1:
    if poziadavky[tuba[l]] > 0:
        splnene[tuba[l]] += 1
        while r < n + 1:
            if poziadavky[tuba[r]] == 0:
                r += 1
            elif splnene[tuba[r]] > 0:
                splnene[tuba[r]] -= 1
                r += 1
            else:
                break

    vysledok = n + l - r + 1
    #print(l, r, vysledok)

    if vysledok < minimum:
        minimum = vysledok

    l += 1

print(minimum)
