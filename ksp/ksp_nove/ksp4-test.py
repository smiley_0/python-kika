word = input()
ptr = 0

SYMBOLS = {'S', 'SV', 'V', 'JV', 'J', 'JZ', 'Z', 'SZ'}
tokens = []

x = 0
while x < len(word) - 1:
    if word[x:x+2] in SYMBOLS:
        tokens.append(word[x:x+2])
        x += 2
    else:
        tokens.append(word[x])
        x += 1

print(tokens)
