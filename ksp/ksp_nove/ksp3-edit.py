n, m, f = [int(i) for i in input().split()]
tuba = [-42] + [int(i) for i in input().split()] + [-42]
poziadavky = [0 for i in range(f + 1)]

for i in range(m):
    a, farba = [int(i) for i in input().split()]
    poziadavky[farba] = a

l, r = 0, 1
splnene = [0 for x in range(f + 1)]
splnene_r = [0 for x in range(f + 1)]

for x in tuba[1:-1]:
    splnene_r[x] += 1
minimum = n

while True:
    while r < n + 1:
        tr = tuba[r]
        if poziadavky[tr] == 0:
            r += 1
        elif splnene[tr] + splnene_r[tr] - 1 >= poziadavky[tr]:
            splnene_r[tr] -= 1
            r += 1
        else:
            break

    vysledok = n + l - r + 1
    minimum = min(minimum, vysledok)

    if l == n:
        break

    l += 1
    if poziadavky[tuba[l]] > 0:
        splnene[tuba[l]] += 1


print(minimum)
