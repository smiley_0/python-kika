vstup = input()[::-1]
vec = 0
s0, sm, s1 = None, None, None

def vec():
    global s0, sm, s1
    poradie = {
        'S': 'V',
        'V': 'J',
        'J': 'Z',
        'Z': 'S'
    }

    if poradie[s0] != s1:
        s0, s1 = s1, s0

def csm():
    global s0, sm, s1

    if s0 == 'V' and s1 == 'J':
        sm = 'JV'
    elif s0 == 'Z' and s1 == 'S':
        sm = 'SZ'
    elif len(s0) <= len(s1):
        sm = s0+s1
    else:
        sm = s1+s0

if vstup == 'S':
    print('0.0')
    exit()
elif vstup == "J":
    print('0.1')
    exit()

if vstup[0] == 'V':
    print('0.0', end='')
    s1 = 'V'
else:
    print('0.1', end='')
    s1 = 'Z'

if len(vstup) == 1:
    print(1)
    exit()


if vstup[1] == 'S':
    s0 = 'S'
    print(0 if s1 == 'V' else 1, end='')
    vec()
    csm()
else:
    s0 = 'J'
    print(1 if s1 == 'V' else 0, end='')
    vec()
    csm()

x = 2
while x < len(vstup):
    if len(s0) >= len(s1):
        if vstup[x:x+len(s0)][::-1].startswith(s0):
            s1 = sm
            print(0, end='')
            x += len(s0)
        elif vstup[x:x+len(s1)][::-1].startswith(s1):
            s0 = sm
            print(1, end='')
            x += len(s1)
    else:
        if vstup[x:x+len(s1)][::-1].startswith(s1):
            s0 = sm
            print(1, end='')
            x += len(s1)
        elif vstup[x:x+len(s0)][::-1].startswith(s0):
            s1 = sm
            print(0, end='')
            x += len(s0)
    csm()

print(1)
