import sys
sys.setrecursionlimit(1000000)


t = int(input())

for i in range(t):
    n = int(input())
    pole = [[] for _ in range(n)]
    navstivene = [False]*(n)

    for i in range(n-1):
        c1, c2 = [int(i) for i in input().split()]
        pole[c1].append(c2)
        pole[c2].append(c1)

    objem = 0

    def funkcia_kt_navstivi_vrchol(vrchol):
        global objem
        navstivene[vrchol] = True
        p = 0 # asi pocet vrcholov
        pp = 0 # suma
        for i in pole[vrchol]:
            if not navstivene[i]:
                pp += funkcia_kt_navstivi_vrchol(i)
                p += 1
        if p == 0:
            objem += 1
            return 1

        rozmer = pp + p + 1
        objem += rozmer
        return rozmer

    funkcia_kt_navstivi_vrchol(0)
    print(objem)
