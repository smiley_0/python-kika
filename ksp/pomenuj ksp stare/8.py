def funkcia(retazec):
    if retazec == '':
        return True

    elif len(retazec) >= 2 and retazec[0] == retazec[-1] and funkcia(retazec.strip(retazec[0])):
        return True

    for i in range(len(retazec)-1):
        u = retazec[:i+1]
        v = retazec[i+1:]
        if funkcia(u) and funkcia(v):
            return True

    if retazec[0] != retazec[-1]:
        return False

    p = retazec[0]
    start = 0
    while start < len(retazec):
        i = retazec.find(p, start+1, len(retazec)-1)

        if i == -1:
            return False

        u = retazec[1:i]
        v = retazec[i+1:-2]
        start = i

        if not (len(u) > 0 and len(v) > 0):
            continue

        if funkcia(u) and funkcia(v):
            return True

    return False


for i in range(int(input())):
    print('ano' if funkcia(input()) else 'nie')
