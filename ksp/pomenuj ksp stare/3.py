n = int(input())
zviera = ''.join([input() for i in range(n)])

zvierata = [
    ('>O+=-', 'mutant'),
    ('<+O=-', 'mutant'),
    ('>O=-', 'had'),
    ('<O=-', 'had'),
    ('@/mOow| ', 'baran'),
    ('@\mOow| ', 'baran'),
    ('_/mOow| ', 'ovca'),
    ('_\mOow| ', 'ovca'),
    ("/\|'oO", 'mutant'),
    ('/\|oO', 'stonozka'),
    ('_<> ', 'ryba'),
    ('^/=O| ', 'macka'),
    ('^\=O| ', 'macka'),
]

stvornohe = {
    'macka',
    'ovca',
    'baran'
}


def funkcia_na_nieco(nieco):
    pass

if 'v' in zviera:
    print('vel', end='')

if zviera.count('O') > 1:
    print('mutant')
    exit()

for znaky, zv in zvierata: # zv je nazov zvierata
    for znak in znaky:
        if znak not in zviera:
            break

    else:
        for nieco in zviera:
            if nieco != 'v' and nieco not in znaky:
                print('mutant')
                exit()

        if zv == 'stonozka' and len(zviera) == 9:
            print('mravec')

        elif zv == 'stonozka' and zviera.count('|')%2 != 0:
            print('mutant')

        elif (zv == 'ryba' or zv == 'velryba') and zviera.count('>') == zviera.count('<'):
            print('mutant')

        elif zv in stvornohe and zviera.count('|') != 4:
            print('mutant')

        elif zv == 'macka' and zviera.count('^') != 2:
            print('mutant')

        else:
            print(zv)

        exit()

print('mutant')
