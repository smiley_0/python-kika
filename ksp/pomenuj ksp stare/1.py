n, m = [int(i) for i in input().split()]
pole = [0]*n

for i in range(m):
    a, b = [int(i) for i in input().split()]

    for j in range(a, b):
        if pole[j] == 0:
            pole[j] = 1
        else:
            print('ANO')
            exit()

print('NIE')
