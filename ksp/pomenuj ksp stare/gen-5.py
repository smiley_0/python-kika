from random import randint

a, b = 131, 11

assert a % 2 != 0
assert b % 2 != 0

m = randint(0, 99)

print(a, b, m)

for row in range(a):
    print(' '.join(str(randint(0, 99)) for _ in range(b)))
