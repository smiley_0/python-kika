n = int(input())
pole = [[] for _ in range(n+1)]
navstivene = [False]*(n+1)

for i in range(n-1):
    c1, c2 = [int(i) for i in input().split()]
    pole[c1].append(c2)
    pole[c2].append(c1)

def funkcia_kt_navstivi_vrchol(vrchol):
    navstivene[vrchol] = True
    p = 0
    pp = 0
    for i in pole[vrchol]:
        if not navstivene[i]:
            pp += funkcia_kt_navstivi_vrchol(i)
            p += 1
    if p == 0:
        return 1
    return pp

print(funkcia_kt_navstivi_vrchol(1))
