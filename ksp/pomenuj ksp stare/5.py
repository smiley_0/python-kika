a, b, m = [int(i) for i in input().split()]

celkovo_m, celkovo_v = 0, 0
pole_m, pole_v = [], []

for i in range(a):
    pole = [int(i) for i in input().split()]

    mensie = 0
    vacsie = 0
    for cislo in pole:
        if cislo < m:
            mensie += 1
            celkovo_m += 1
        elif cislo > m:
            vacsie += 1
            celkovo_v += 1

    pole_m.append(mensie)
    pole_v.append(vacsie)

if celkovo_m > celkovo_v:
    cpole = pole_m
    zmenit = celkovo_m - (a*b)//2
elif celkovo_m < celkovo_v:
    cpole = pole_v
    zmenit = celkovo_v - (a*b)//2
else:
    print(0)
    exit()

cpole.sort(reverse=True)
ludi = 0
for clovek in cpole:
    if zmenit <= 0:
        break

    zmenit -= clovek
    ludi += 1

print(ludi)
