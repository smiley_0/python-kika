from itertools import combinations

n = int(input())
draty = [int(i) for i in input().split()]

def porovnaj(a, b):
    return (a[0] < b[0] and a[1] > b[1]) or (a[0] > b[0] and a[1] < b[1])

def neviem(pole, a):
    for p in pole:
        col = False
        for b in p:
            if porovnaj(b, a):
                col = True
                break
        if not col:
            p.add(a)
            return

    pole.append({a})


pary = []
for a, b in enumerate(draty):
    neviem(pary, (a+1, b))

print(len(pary))
