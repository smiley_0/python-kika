b, n = [int(i) for i in input().split()]

if n==1:
    print(1)
    exit()

def robenie_cisla(b, n, x):
    cislo = [x]
    zvysok = 0

    while True:
        if len(cislo) > 2 and len(cislo) % 2 == 0:
            if cislo[(len(cislo) // 2)-1] == cislo[-1] and cislo[(len(cislo) // 2)-2] == cislo[-2]:
            #if cislo[:len(cislo) // 2] == cislo[len(cislo) // 2:]:
                break

        a = n * cislo[-1] + zvysok
        cx = a % b
        zvysok = (a-cx)//b
        cislo.append(cx)

    cislo = cislo[:len(cislo) // 2]
    for x, i in enumerate(cislo[::-1]):
        if i != 0:
            break
    return cislo[:len(cislo)-x]

def nasobenie(b, n, cislo):
    zvysok = 0
    nove = []

    for i in cislo:
        a = n * i + zvysok
        cx = a % b
        zvysok = (a-cx)//b
        nove.append(cx)
    if zvysok != 0:
        nove.append(zvysok)
    return nove

def konverzia(b, cislo):
    des = 0
    for i in range(len(cislo)):
        des += cislo[i]*(b**i)

    return des

min_cislo = None
min_len = None

for i in range(1, b):
    rc = robenie_cisla(b, n, i)
    nas = nasobenie(b, n, rc)
    shift = rc[1:] + [rc[0]]

    #print(i, rc, nas, shift)
    #if nas == shift:
    if nas[-1] == shift[-1] and nas[-2] == shift[-2]:
        if min_len is None or len(rc) < min_len:
            min_len = len(rc)
            min_cislo = rc
        elif len(rc) == min_len and rc[-1] < min_cislo[-1]:
            min_len = len(rc)
            min_cislo = rc

print(' '.join(str(x) for x in min_cislo[::-1]))
