n = int(input())
heslo = [input() for i in range(n)]

vysledok = 1
for i in range(len(heslo[0])):
    a = set()
    for slovo in heslo:
        a.add(slovo[i])

    vysledok *= 26 - len(a)


print(vysledok)
