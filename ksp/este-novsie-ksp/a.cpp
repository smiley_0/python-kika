#include <iostream>

static inline int NumDigits(int x)
{
    x = abs(x);
    return (x < 10 ? 1 :
           (x < 100 ? 10 :
           (x < 1000 ? 100 :
           (x < 10000 ? 1000 :
           (x < 100000 ? 10000 :
           (x < 1000000 ? 100000 :
           (x < 10000000 ? 1000000 :
           (x < 100000000 ? 10000000 :
           (x < 1000000000 ? 100000000 :
           1000000000)))))))));
}

static inline int shiftr(int n) {
    return (n / 10) + (n % 10) * NumDigits(n);
}

int main(int argc, const char** argv) {
    int n = 2;
    for (long int i = 0; i < 10000000000; i++) {
        if (i * n == shiftr(i)) {
            std::cout << i << std::endl;
        }
    }
    return 0;
}
