from itertools import product

n, k = [int(i) for i in input().split()]
rozvrhy = [input() for i in range(n)]

def funkcia(a, b):
    p = 0
    for x, y in zip(a, b):
        if x != y:
            p += 1

    return p

x = 0
r = None
for jozko in product('01', repeat=k):
    n = 50
    for rozvrh in rozvrhy:
        n = min(funkcia(rozvrh, jozko), n)

    if n > x:
        r = jozko
        x = n

print(''.join(r))
