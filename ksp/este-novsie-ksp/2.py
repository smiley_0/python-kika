n = int(input())
veduci = []
sedenie = [-1 for i in range(n+1)]

for i, cislo in enumerate(input().split()):
    cislo = int(cislo)
    veduci.append([cislo, False, False])  #[miesto, chce odist, odisiel]
    sedenie[cislo] = i

# print(veduci, sedenie)

poradie = [int(i) for i in input().split()]

real_poradie = []
z, k = 0, n - 1
for cislo in poradie:
    veduci[sedenie[cislo]][1] = True
    #print(f'{cislo}: mna to nebavi')

    while z < n and veduci[z][1] == True and not veduci[z][2]:
        veduci[z][2] = True
        real_poradie.append(veduci[z][0])
        #print(f'left - {veduci[z]}')
        z += 1

    while k >= 0 and veduci[k][1] == True and not veduci[k][2]:
        veduci[k][2] = True
        real_poradie.append(veduci[k][0])
        #print(f'right - {veduci[k]}')
        k -= 1

print(*real_poradie)
