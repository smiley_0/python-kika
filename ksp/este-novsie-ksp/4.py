b, n = [int(i) for i in input().split()]
cislo = [0]

def in_kremacia(cislo):
    cislo[0] += 1

    for i in range(len(cislo)):
        if cislo[i] >= b:
            # Prenos do vyssieho radu
            if i+1 >= len(cislo):
                cislo.append(1)
            else:
                cislo[i+1] += 1

            cislo[i] -= b


def ro_tacia(cislo):
    return cislo[1:] + [cislo[0]]

def ko_ntroverzia(cislo):
    des = 0
    for i in range(len(cislo)):
        des += cislo[i]*(b**i)

    return des

while True:
    in_kremacia(cislo)
    #print(cislo, ro_tacia(cislo), '->', ko_ntroverzia(cislo), ko_ntroverzia(ro_tacia(cislo)))

    if ko_ntroverzia(cislo)*n == ko_ntroverzia(ro_tacia(cislo)):
        break

print(*cislo[::-1])
