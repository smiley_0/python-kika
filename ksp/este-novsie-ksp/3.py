import sys
sys.setrecursionlimit(1000000)

n, m = [int(i) for i in input().split()]
susedia = [set() for i in range(n)]
navstivene = [False]*(n)

for i in range(m):
    a, b = [int(i) for i in input().split()]
    susedia[a].add(b)
    susedia[b].add(a)

    if len(susedia[a]) > 2 or len(susedia[b]) > 2:
        print('nie')
        exit()

k = []
def dfs(vrchol):
    k.append(vrchol)
    navstivene[vrchol] = True
    for i in susedia[vrchol]:
        if not navstivene[i]:
            dfs(i)

komponenty = []
for i, neviem in enumerate(navstivene):
    if neviem == False:
        k = []
        dfs(i)
        komponenty.append(k)

for k in komponenty:
    dvojkove = 0
    nedvojkove = 0

    for cislo in k:
        if len(susedia[cislo]) == 2:
            dvojkove += 1
        else:
            nedvojkove += 1

    if nedvojkove == 0 and len(komponenty) > 1:
        print('nie')
        exit()

print('ano')
