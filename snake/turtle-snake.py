#!/usr/bin/env python3

import turtle as t

BLOCK_SIZE = 20
MAP_W = 20
MAP_H = 20

y = 0

def draw_rect(x, y, w, h):
    t.pu()
    t.goto(x, y)
    t.pd()
    t.begin_fill()
    t.goto(x+w, y)
    t.goto(x+w, y+h)
    t.goto(x, y+h)
    t.end_fill()

def yyy():
    global y
    y += 1
    print(y)

def loop():
    global y
    print("Run")
    t.clear()
    for x in range(10):
        draw_rect(x*BLOCK_SIZE, y, BLOCK_SIZE, BLOCK_SIZE)

def main():
    t.speed(0)
    t.ht()
    t.tracer(10)
    t.ontimer(loop, 16)
    t.onkeypress(yyy, "Up")
    t.listen()
    t.mainloop()

if __name__ == '__main__':
    main()
