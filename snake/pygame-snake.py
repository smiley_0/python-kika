#!/usr/bin/env python3

import pygame
import sys
from random import randint
from enum import Enum, auto
from collections import namedtuple, deque

pygame.init()
pygame.font.init()

# Consts
BLOCK_SIZE = 20
MAP_W = 32
MAP_H = 32
STEP_TIME = 450
LEVEL_SCORES = {5, 10, 20, 30, 42, 55, 70, 85, 100, 120, 160, 200}

# Colors
BLACK = 0, 0, 0
GRAY = 100, 100, 100
LIGHT_GRAY = 200, 200, 200
WHITE = 255, 255, 255
RED = 255, 60, 60
PY_BLUE = 69, 132, 182

# Entities
class Position(object):
    __slots__ = ('x', 'y')
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    @staticmethod
    def random():
        return Position(randint(0, MAP_W-1), randint(0, MAP_H-1))


class Snake(object):
    __slots__ = ('heading', 'blocks', 'length')
    def __init__(self, heading, blocks=[], length=None):
        # Generate random start
        if len(blocks) <= 0:
            blocks.append(Position.random())

        self.heading = heading
        self.blocks = deque(blocks)
        self.length = len(blocks) if length is None else length

    def move(self):
        head = self.blocks[0]
        mx, my = SNAKE_MOVEMENT[self.heading]
        self.blocks.appendleft(Position(head.x+mx, head.y+my))
        snake.fix_length()

    def check_self_collisions(self):
        head = self.blocks[0]
        for i, block in enumerate(self.blocks):
            # Cannot slice deque
            if i > 0 and head == block:
                return True

        return False

    def fix_length(self):
        while len(self.blocks) > self.length:
            self.blocks.pop()

class GameState(Enum):
    RUNNING = auto()
    PAUSED = auto()
    OVER = auto()


class Direction(Enum):
    UP = auto()
    LEFT = auto()
    DOWN = auto()
    RIGHT = auto()


# Controls
SNAKE_CONTROLS = {
    pygame.K_w: Direction.UP,
    pygame.K_a: Direction.LEFT,
    pygame.K_s: Direction.DOWN,
    pygame.K_d: Direction.RIGHT,
    pygame.K_UP: Direction.UP,
    pygame.K_LEFT: Direction.LEFT,
    pygame.K_DOWN: Direction.DOWN,
    pygame.K_RIGHT: Direction.RIGHT,
}

SNAKE_MOVEMENT = {
    Direction.UP: (0, -1),
    Direction.LEFT: (-1, 0),
    Direction.DOWN: (0, 1),
    Direction.RIGHT: (1, 0),
}

# PyGame Config
size = width, height = (MAP_W+2) * BLOCK_SIZE, (MAP_H+3) * BLOCK_SIZE
screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()
font = pygame.font.SysFont('Comic Sans MS', 16)
font_large = pygame.font.SysFont('Comic Sans MS', 42)

# Functions
def get_rect_from_position(pos: Position):
    return pygame.Rect(
        (pos.x+1) * BLOCK_SIZE,
        (pos.y+1) * BLOCK_SIZE,
        BLOCK_SIZE, BLOCK_SIZE
    )

def check_wall_collisions(snake):
    head = snake.blocks[0]
    return head.x < 0 or head.y < 0 or head.x >= MAP_W or head.y >= MAP_H

def render_text(screen, text, font, coords, color=WHITE, align='left'):
    x_offset = 0

    if align == 'right':
        x_offset = font.size(text)[0]
    elif align == 'center':
        x_offset = font.size(text)[0]//2

    txt = font.render(text, True, color)

    new_coords = coords[0]-x_offset, coords[1]
    screen.blit(txt, new_coords)

# Init
game_frame = pygame.Rect(BLOCK_SIZE, BLOCK_SIZE, MAP_W*BLOCK_SIZE, MAP_H*BLOCK_SIZE)

# Game vars
snake = Snake(Direction.UP, [Position(MAP_W//2, MAP_H//2)], length=4)
food = Position.random()
game_state = GameState.RUNNING
score = 0

# Timing
elapsed = 0
speed = 1

# Main loop
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                sys.exit()
            elif event.key == pygame.K_p:
                if game_state is GameState.RUNNING:
                    game_state = GameState.PAUSED
                elif game_state is GameState.PAUSED:
                    game_state = GameState.RUNNING
            elif event.key in SNAKE_CONTROLS.keys():
                snake.heading = SNAKE_CONTROLS[event.key]

    # Recalculate
    if game_state is GameState.RUNNING:
        elapsed += clock.get_time()
        if elapsed * speed >= STEP_TIME:
            elapsed -= STEP_TIME / speed

            snake.move()

            if check_wall_collisions(snake) or snake.check_self_collisions():
                print('Game over')
                game_state = GameState.OVER

            if snake.blocks[0] == food:
                score += 1
                snake.length += 1

                # Level up
                if score in LEVEL_SCORES:
                    speed += 1
                    snake.length -= speed

                food = Position.random()

            snake.fix_length()

    # Render
    screen.fill((0, 0, 0))
    pygame.draw.rect(screen, GRAY, game_frame, 1)

    # Draw snake
    for i, block in enumerate(snake.blocks):
        color = WHITE if i == 0 else LIGHT_GRAY
        pygame.draw.rect(screen, color, get_rect_from_position(block))

    # Draw food
    pygame.draw.rect(screen, RED, get_rect_from_position(food))

    # Draw statusbar
    vpos = (MAP_H+1.25)*BLOCK_SIZE
    render_text(screen, 'Score: {}'.format(score), font, (BLOCK_SIZE, vpos))
    render_text(screen, 'Length: {}'.format(snake.length), font, (BLOCK_SIZE + 100, vpos))
    render_text(screen, 'Speed: {}'.format(speed), font, (BLOCK_SIZE + 220, vpos))
    render_text(screen, 'Powered by Python', font, (width - BLOCK_SIZE, vpos), color=PY_BLUE, align='right')

    # Draw overlays
    if game_state is GameState.OVER:
        render_text(screen, 'GAME OVER', font_large, (width//2, height//2), align='center')

    if game_state is GameState.PAUSED:
        render_text(screen, 'PAUSED', font_large, (width//2, height//2), align='center')

    pygame.display.flip()
    clock.tick()
