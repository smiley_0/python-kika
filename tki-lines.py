import tkinter as tk
from math import sin, pi

width, height = 800, 600
blocksize = 50

def sinblocksize(i, ma):
    print(i, ma, i*blocksize/ma)
    return blocksize*sin((i*blocksize/ma)*pi)

root = tk.Tk()
s = tk.Canvas(root, width=width, height=height, bg='black')

rects = []
for j in range(width//blocksize):
    for i in range(height//blocksize):
        rects.append(s.create_rectangle(
            j*blocksize, (i*blocksize)+sinblocksize(i, height),
            (j+1)*blocksize, ((i+1)*blocksize)-sinblocksize(i, height),
            fill='blue'
        ))

s.pack()
root.mainloop()
