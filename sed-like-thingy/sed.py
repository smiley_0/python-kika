import sys

_, file, what, to = sys.argv

with open(file, 'r+') as f:
    content = f.read().replace(what, to)
    f.seek(0)
    f.write(content)
    f.truncate()
