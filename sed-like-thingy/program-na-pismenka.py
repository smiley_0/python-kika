from random import choice

with open('pismena.txt', 'r') as f:
    pismena = f.readline().strip()

pole = [pismena[i] for i in range(len(pismena))]
nieco = choice(pole)
pole.pop(pole.index(nieco))

prepis = ''.join(pole)

with open('pismena.txt', 'w') as f:
    f.write(prepis)

print(nieco)
