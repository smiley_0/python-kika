def pocitadlo(s, x, y):
    return s.count(x), s.count(y)

def stranovadlo(x):
    prave = {']', ')', '}'}
    if x in prave:
        return True
    else:
        return False


def otacadlo(x):
    if x == '(':
        return ')'
    elif x == ')':
        return '('
    elif x == '{':
        return '}'
    elif x == '}':
        return '{'
    elif x == '[':
        return ']'
    else:
        return '['

def urcovadlo(nieco, x, y):
    if nieco[0] > nieco[1]:
        return y
    elif nieco[0] < nieco[1]:
        return x
    else:
        return None

def overovadlo(s):
    last_error = None
    pole = []
    try:
        for pos, znak in enumerate(s):
            #print(pos, znak)
            if znak == '[' or znak == '{' or znak == '(':
                pole.append((znak, pos))
            elif znak == ']' and pole[-1][0] == '[':
                pole.pop()
            elif znak == ')' and pole[-1][0] == '(':
                pole.pop()
            elif znak == '}' and pole[-1][0] == '{':
                pole.pop()
            else:
                #print('error invalid', pole[-1])
                last_error = pole[-1]
                while pole[-1][0] != otacadlo(znak):
                    pole.pop()
                pole.pop()

            #print(pole)
    except:
        #print('error empty stack')
        return (False, (znak, pos))

    #print('lasterr', last_error)
    if last_error is None and len(pole) == 0:
        return True
    elif len(pole) == 0:
        return (False, last_error)
    else:
        return (False, pole[-1])

for i in range(int(input())):
    vstup = input()
    print(overovadlo(vstup))
    continue
    kucerave = pocitadlo(vstup, '{', '}')
    hranate = pocitadlo(vstup, '[', ']')
    oble = pocitadlo(vstup, '(', ')')

    #print(kucerave, hranate, oble)

    missing = None
    if missing is None:
        missing = urcovadlo(kucerave, '{', '}')

        if missing is None:
            missing = urcovadlo(hranate, '[', ']')

            if missing is None:
                missing = urcovadlo(oble, '(', ')')

    #print(missing)
    n = []
    for i in range(len(vstup)):
        if vstup[i] == otacadlo(missing):
            n.append(i)

    print(n)
    p = 0
    if stranovadlo(missing):
        for i in range(n[0], len(vstup)+1):
            x = vstup[:n[0]] + vstup[n[0]:i] + missing + vstup[i:]
            #print(x)
            if overovadlo(x):
                p += 1
    else:
        for i in range(n[-1]+1):
            x = vstup[:i] + missing + vstup[i:]
            if overovadlo(x):
                p += 1
    print(p)
