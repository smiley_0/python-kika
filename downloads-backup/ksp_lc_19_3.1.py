n, q = (int(i) for i in input().split())

aplikacie = []
p = 0
ppv = [0 for i in range(n+1)]
dp = [0 for i in range(n+1)]
pocet = 0

for i in range(q):
    udalost, x_t = (int(i) for i in input().split())

    if udalost == 1:
        aplikacie.append(x_t)
        dp[x_t] += 1
        pocet += 1
    elif udalost == 2:
        ppv[x_t] = len(aplikacie)
        pocet -= dp[x_t]
        dp[x_t] = 0
    else:
        odstranene = 0
        while odstranene < x_t:
            if ppv[aplikacie[p]] <= p:
                pocet -= 1
                dp[aplikacie[p]] -= 1
                #('removing', aplikacie[p], 'count', dp[aplikacie[p]])
                odstranene += 1
            
            p += 1
        

    print(pocet)
    #print(aplikacie[p:])