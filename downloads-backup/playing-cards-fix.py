_, value, _, suit = input('Zadaj nazov karty.\nformat: the VALUE of SUIT\n(hearts/diamonds/spades/clubs)').lower().split()

##hodnoty kariet
cisla = {
    'two': '2',
    'three': '3',
    'four': '4',
    'five': '5',
    'six': '6',
    'seven': '7',
    'eight': '8',
    'nine': '9',
    'ten': '10',
    'jack': 'J',
    'queen': 'Q',
    'king': 'K',
    'ace': 'A'
}

znak = {
    'hearts': [' # # ','#####','#####',' ### ','  #  '],
    'diamonds': ['  #  ',' ### ','#####',' ### ','  #  '],
    'spades': ['  #  ',' ### ','#####','# # #','  #  '],
    'clubs': [' ### ','  #  ','#####','# # #','  #  ']
}

suradnice = {
    'two': [(5, 10), (19, 10)],
    'three': [(12, 10), (5, 10), (19, 10)],
    'four': [(9, 2), (9, 18), (16, 2), (16, 18)],
    'five': [(12, 10), (9, 2), (9, 18), (16, 2), (16, 18)],
    'six': [(9, 2), (9, 18), (16, 2), (16, 18), (5, 10), (19, 10)],
    'seven': [(9, 2), (9, 18), (16, 2), (16, 18), (12, 10), (5, 10), (19, 10)],
    'eight': [(2, 2), (2, 18), (9, 2), (9, 18), (16, 2), (16, 18), (23, 2), (23, 18)],
    'nine': [(2, 2), (2, 18), (9, 2), (9, 18), (12, 10), (16, 2), (16, 18), (23, 2), (23, 18)],
    'ten': [(2, 2), (2, 18), (9, 2), (9, 18), (16, 2), (5, 10), (19, 10), (16, 18), (23, 2), (23, 18)],
    'jack': [(12, 10)],
    'queen': [(12, 10)],
    'king': [(12, 10)],
    'ace': [(12, 10)]
}

num_positions = [(1, 1), (1, 23), (28, 1), (28, 23)]
num_positions2 = [(1, 1), (1, 22), (28, 1), (28, 22)]

##prazdna karta
karta_vzhlad = [[' ' for j in range(25)] for i in range(30)]
#print(karta_vzhlad)

for i in range(len(karta_vzhlad)):
    for j in range(len(karta_vzhlad[i])):
        if i == 0 or i == 29:
            if j == 0 or j == 24:
                karta_vzhlad[i][j] = '+'

            else:
                karta_vzhlad[i][j] = '-'

        else:
            karta_vzhlad[i][0] = '|'
            karta_vzhlad[i][24] = '|'


##miesto, kam davam znak
def poloha(tvar, y, x):      ##v ktorom rohu zacinam kreslit
    for r, row in enumerate(tvar):
        for c, char in enumerate(row):
            karta_vzhlad[y+r][x+c] = char

def karta_znaky(cislo_karty, znak):  ##value
    for coords in suradnice[cislo_karty]:
        poloha(znak, *coords)

##cislo karty
p = num_positions2 if value == 'ten' else num_positions
for pos in p:
    poloha([cisla[value]], *pos)
#print(karta_vzhlad)

karta_znaky(value, znak[suit])

##vykreslovanie karty
#print(karta_vzhlad)
for i in range(len(karta_vzhlad)):
    print(''.join(karta_vzhlad[i]))
