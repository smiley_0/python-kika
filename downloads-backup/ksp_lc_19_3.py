n, q = (int(i) for i in input().split())

aplikacie = [0 for i in range(n+1)]

for i in range(q):
    udalost, x_t = (int(i) for i in input().split())

    if udalost == 1:
        aplikacie[x_t] += 1
    elif udalost == 2:
        aplikacie[x_t] = 0
    else:
        print('event 3')
        while x_t > 0:
            for j in aplikacie:
                if j > 0:
                    if x_t > 0:
                        j -= 1
                        x_t -= 1
                    else:
                        break

    pocet = 0

    for k in aplikacie:
        pocet += k

    print(pocet)
