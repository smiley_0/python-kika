karta = input('Zadaj nazov karty.\nformat: the VALUE of SUIT\n(hearts/diamonds/spades/clubs)').split()


##hodnoty kariet
cisla = {'two': '2',
        'three': '3',
        'four': '4',
        'five': '5',
        'six': '6',
        'seven': '7',
        'eight': '8',
        'nine': '9',
        'ten': ['1','0'],
        'jack': 'J',
        'queen': 'Q',
        'king': 'K',
        'ace': 'A'}

znak = {'hearts': [' # # ','#####','#####',' ### ','  #  '],
        'diamonds': ['  #  ',' ### ','#####',' ### ','  #  '],
        'spades': ['  #  ',' ### ','#####','# # #','  #  '],
        'clubs': [' ### ','  #  ','#####','# # #','  #  ']}


##prazdna karta
karta_vzhlad = [[' ' for j in range(25)] for i in range(30)]
#print(karta_vzhlad)

for i in range(len(karta_vzhlad)):
    for j in range(len(karta_vzhlad[i])):
        if i == 0 or i == 29:
            if j == 0 or j == 24:
                karta_vzhlad[i][j] = '+'

            else:
                karta_vzhlad[i][j] = '-'

        else:
            karta_vzhlad[i][0] = '|'
            karta_vzhlad[i][24] = '|'


##cislo karty
if karta[1] == 'ten':
    ##prvy riadok
    karta_vzhlad[1].pop(1)
    karta_vzhlad[1].insert(1, cisla[karta[1]][0])
    karta_vzhlad[1].pop(2)
    karta_vzhlad[1].insert(2, cisla[karta[1]][1])
    karta_vzhlad[1].pop(22)
    karta_vzhlad[1].insert(22, cisla[karta[1]][0])
    karta_vzhlad[1].pop(23)
    karta_vzhlad[1].insert(23, cisla[karta[1]][1])
    ##posledny riadok
    karta_vzhlad[28].pop(1)
    karta_vzhlad[28].insert(1, cisla[karta[1]][0])
    karta_vzhlad[28].pop(2)
    karta_vzhlad[28].insert(2, cisla[karta[1]][1])
    karta_vzhlad[28].pop(22)
    karta_vzhlad[28].insert(22, cisla[karta[1]][0])
    karta_vzhlad[28].pop(23)
    karta_vzhlad[28].insert(23, cisla[karta[1]][1])

else:
    ##prvy riadok
    karta_vzhlad[1].pop(1)
    karta_vzhlad[1].insert(1, cisla[karta[1]])
    karta_vzhlad[1].pop(23)
    karta_vzhlad[1].insert(23, cisla[karta[1]])
    ##posledny riadok
    karta_vzhlad[28].pop(1)
    karta_vzhlad[28].insert(1, cisla[karta[1]])
    karta_vzhlad[28].pop(23)
    karta_vzhlad[28].insert(23, cisla[karta[1]])
#print(karta_vzhlad)


##miesto, kam davam znak
def poloha(tvar,x,y):      ##v ktorom rohu zacinam kreslit
    for i in range(len(znak[karta[3]])):  ##riadky
        for j in range(len(znak[karta[3]][i])):  ##stlpce
            karta_vzhlad[x].pop(y)
            karta_vzhlad[x].insert(y, znak[karta[3]][i][j])  ##vsetko mi to vezme ale nezapise mi to do vyslednej karty
            print(karta_vzhlad[x])
            #print(znak[karta[3]][i][j])
            #print('tu:', karta_vzhlad[x][y])
            #print('_____')
        x += 1
    return(karta_vzhlad)

poloha(karta[3],2,2)


##vykreslovanie karty
for i in range(len(karta_vzhlad)):
    print(''.join(karta_vzhlad[i]))

