import tkinter as tk
from math import gcd

a = tk.Tk()
a.geometry("400x200")

lab1 = tk.Label(a,text="prve cislo")
lab1.grid(row=0)

c1 = tk.Entry()
c1.grid(row=0, column=1)

lab2 = tk.Label(a,text="druhe cislo")
lab2.grid(row=1)

c2 = tk.Entry()
c2.grid(row=1, column=1)

vysledok = tk.Label(a, text="vysledok")
vysledok.grid(row=2)

sucet = tk.Button(a, text="NSD", command=lambda: vysledok.config(text=f'{gcd(int(c1.get()), int(c2.get()))}'))
sucet.grid(row=3)

a.mainloop()
