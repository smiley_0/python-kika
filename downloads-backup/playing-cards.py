karta = input('Zadaj nazov karty.\nformat: the VALUE of SUIT\n(hearts/diamonds/spades/clubs)').split()

##hodnoty kariet
znak = {'two': '2',
        'three': '3',
        'four': '4',
        'five': '5',
        'six': '6',
        'seven': '7',
        'eight': '8',
        'nine': '9',
        'ten': '10',
        'jack': 'J',
        'queen': 'Q',
        'king': 'K',
        'ace': 'A'}

hearts = ' # # \n#####\n ###  \n  #   '
diamonds = '  #  \n ### \n#####\n ### \n  #  '
spades = '  #  \n ### \n#####\n# # #\n  #  '
clubs = ' ### \n  #  \n#####\n# # #\n  #  '

##prazdna karta
karta_vzhlad = [[' ' for j in range(25)] for i in range(30)]
#print(karta_vzhlad)

for i in range(len(karta_vzhlad)):
    for j in range(len(karta_vzhlad[i])):
        if i == 0 or i == 29:
            if j == 0 or j == 24:
                karta_vzhlad[i][j] = '+'

            else:
                karta_vzhlad[i][j] = '-'

        else:
            karta_vzhlad[i][0] = '|'
            karta_vzhlad[i][24] = '|'


##cislo karty
karta_vzhlad[1][1] = znak[karta[1]]
karta_vzhlad[1][23] = znak[karta[1]]
karta_vzhlad[28][1] = znak[karta[1]]
karta_vzhlad[28][23] = znak[karta[1]]
#print(karta_vzhlad)

if znak[karta[1]] % 2 == 0:
    #tu chcem aby mi to vykreslilo znaky parnych cisel, takze bez stredneho znaku
    #ak 2, znak spravi na nejakych indexoch
else:
    #tu to bude robit aj ten stredny znak
    #ak 3, tak mi spravi znak na takychto indexoch



for i in range(len(karta_vzhlad)):
    print(''.join(karta_vzhlad[i]))