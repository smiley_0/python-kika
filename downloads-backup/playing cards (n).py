karta = input('Zadaj nazov karty.\nformat: the VALUE of SUIT\n(hearts/diamonds/spades/clubs)').split()


##hodnoty kariet
cisla = {'two': '2',
        'three': '3',
        'four': '4',
        'five': '5',
        'six': '6',
        'seven': '7',
        'eight': '8',
        'nine': '9',
        'ten': ['1','0'],
        'jack': 'J',
        'queen': 'Q',
        'king': 'K',
        'ace': 'A'
        }

znak = {'hearts': [' # # ','#####',' ### ','  #  '],
        'diamonds': ['  #  ',' ### ','#####',' ### ','  #  '],
        'spades': ['  #  ',' ### ','#####','# # #','  #  '],
        'clubs': [' ### ','  #  ','#####','# # #','  #  ']
        }


##prazdna karta
karta_vzhlad = [[' ' for j in range(25)] for i in range(30)]
#print(karta_vzhlad)

for i in range(len(karta_vzhlad)):
    for j in range(len(karta_vzhlad[i])):
        if i == 0 or i == 29:
            if j == 0 or j == 24:
                karta_vzhlad[i][j] = '+'

            else:
                karta_vzhlad[i][j] = '-'

        else:
            karta_vzhlad[i][0] = '|'
            karta_vzhlad[i][24] = '|'


##cislo karty
if karta[1] == 'ten':
    ##prvy riadok
    karta_vzhlad[1][1] = cisla[karta[1]][0]
    karta_vzhlad[1][2] = cisla[karta[1]][1]
    karta_vzhlad[1][22] = cisla[karta[1]][0]
    karta_vzhlad[1][23] = cisla[karta[1]][1]

    ##posledny riadok
    karta_vzhlad[28][1] = cisla[karta[1]][0]
    karta_vzhlad[28][2] = cisla[karta[1]][1]
    karta_vzhlad[28][22] = cisla[karta[1]][0]
    karta_vzhlad[28][23] = cisla[karta[1]][1]

else:
    ##prvy riadok
    karta_vzhlad[1][1] = cisla[karta[1]]
    karta_vzhlad[1][23] = cisla[karta[1]]
    ##posledny riadok
    karta_vzhlad[28][1] = cisla[karta[1]]
    karta_vzhlad[28][23] = cisla[karta[1]]
#print(karta_vzhlad)


##miesto, kam davam znak
def poloha(tvar,x,y):      ##v ktorom rohu zacinam kreslit
    for i in range(len(znak[karta[3]])):  ##riadky
        z = y

        for j in range(len(znak[karta[3]][i])):  ##stlpce
            karta_vzhlad[x][z] = znak[karta[3]][i][j]
            z += 1

        x += 1
    return(karta_vzhlad)
#poloha(karta[3],5,5)

##suradnice na vykreslovanie znakov pri roznych hodnotach kariet
suradnice = {'two': [[5,10],[19,10]],
        'three': [[12,10],[5,10],[19,10]],
        'four': [[9,2],[9,18],[16,2],[16,18]],
        'five': [[12,10],[9,2],[9,18],[16,2],[16,18]],
        'six': [[9,2],[9,18],[16,2],[16,18],[5,10],[19,10]],
        'seven': [[9,2],[9,18],[16,2],[16,18],[12,10],[5,10],[19,10]],
        'eight': [[2,2],[2,18],[9,2],[9,18],[16,2],[16,18],[23,2],[23,18]],
        'nine': [[2,2],[2,18],[9,2],[9,18],[12,10],[16,2],[16,18],[23,2],[23,18]],
        'ten': [[2,2],[2,18],[9,2],[9,18],[16,2],[5,10],[19,10],[16,18],[23,2],[23,18]],
        'jack': [[12,10]],
        'queen': [[12,10]],
        'king': [[12,10]],
        'ace': [[12,10]]
        }


##dokreslovanie suits na kartu, podla jej hodnoty
def karta_znaky(cislo_karty):  ##karta[1]
    for i in range(len(suradnice[cislo_karty])):
        for j in range(len(suradnice[cislo_karty][i])):
            poloha(karta[3],suradnice[cislo_karty][i][0], suradnice[cislo_karty][i][1])
    return(karta_vzhlad)

karta_znaky(karta[1])


##vykreslovanie karty
#print(karta_vzhlad)
for i in range(len(karta_vzhlad)):
    print(''.join(karta_vzhlad[i]))