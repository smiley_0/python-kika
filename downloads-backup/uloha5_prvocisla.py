f = open('CISLA.txt', 'r')
cisla = []

for line in f:
    cisla += line.strip().split()

cisla = [int(i) for i in cisla]

f.close()

def prvocislo(n):
    p = 0
    for i in range(1, n):
        if n%i == 0:
            p += 1
    if p == 1:
        print(n, 'je prvocislo')
    else:
        print(n, 'nie je prvocislo')

for i in cisla:
    prvocislo(i)

