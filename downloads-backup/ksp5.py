def pocitadlo(s, x, y):
    return s.count(x), s.count(y)

def urcovadlo(nieco, x, y):
    if nieco[0] > nieco[1]:
        return y
    elif nieco[0] < nieco[1]:
        return x
    else:
        return None

def overovadlo(s):
    pole = []
    try:
        for znak in s:
            if znak == '[' or znak == '{' or znak == '(':
                pole.append(znak)
            elif znak == ']' and pole[-1] == '[':
                pole.pop()
            elif znak == ')' and pole[-1] == '(':
                pole.pop()
            elif znak == '}' and pole[-1] == '{':
                pole.pop()
            else:
                return False
    except:
        return False

    return True

for i in range(int(input())):
    vstup = input()
    kucerave = pocitadlo(vstup, '{', '}')
    hranate = pocitadlo(vstup, '[', ']')
    oble = pocitadlo(vstup, '(', ')')

    print(kucerave, hranate, oble)

    missing = None
    if missing is None:
        missing = urcovadlo(kucerave, '{', '}')

        if missing is None:
            missing = urcovadlo(hranate, '[', ']')

            if missing is None:
                missing = urcovadlo(oble, '(', ')')

    p = 0
    for i in range(len(vstup)+1):
        x = vstup[:i] + missing + vstup[i:]
        print(x)
        if overovadlo(x):
            p += 1

    print(p)
