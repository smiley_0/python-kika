karta = input('Zadaj nazov karty.\nformat: the VALUE of SUIT\n(hearts/diamonds/spades/clubs)').split()

##hodnoty kariet
cisla = {'two': '2',
        'three': '3',
        'four': '4',
        'five': '5',
        'six': '6',
        'seven': '7',
        'eight': '8',
        'nine': '9',
        'ten': '10',
        'jack': 'J',
        'queen': 'Q',
        'king': 'K',
        'ace': 'A'}

znak = {'hearts': [' # # ','#####','#####',' ### ','  #  '],
        'diamonds': ['  #  ',' ### ','#####',' ### ','  #  '],
        'spades': ['  #  ',' ### ','#####','# # #','  #  '],
        'clubs': [' ### ','  #  ','#####','# # #','  #  ']}

##prazdna karta
karta_vzhlad = [[' ' for j in range(25)] for i in range(30)]
#print(karta_vzhlad)

for i in range(len(karta_vzhlad)):
    for j in range(len(karta_vzhlad[i])):
        if i == 0 or i == 29:
            if j == 0 or j == 24:
                karta_vzhlad[i][j] = '+'

            else:
                karta_vzhlad[i][j] = '-'

        else:
            karta_vzhlad[i][0] = '|'
            karta_vzhlad[i][24] = '|'


##cislo karty
karta_vzhlad[1][1] = cisla[karta[1]]
karta_vzhlad[28][1] = cisla[karta[1]]

if karta[1] == 'ten':
    karta_vzhlad[1][22] == cisla[karta[1]]
    karta_vzhlad[28][22] = cisla[karta[1]]
    print('tu')
else:
    karta_vzhlad[1][23] = cisla[karta[1]]
    karta_vzhlad[28][23] = cisla[karta[1]]
    print('tam')
#print(karta_vzhlad)

##miesto, kam davam znak
def poloha1(tvar):      ##[2][2]-[6][6]
    e = 0
    for i in range(len(znak[karta[3]])):
        for j in range(len(znak[karta[3]][i])):
            karta_vzhlad[e+2][j+2] = znak[karta[3]][e][j]
        e += 1
    return(karta_vzhlad)
poloha1(karta[3])

##vykreslovanie karty
for i in range(len(karta_vzhlad)):
    print(''.join(karta_vzhlad[i]))
