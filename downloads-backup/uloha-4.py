from string import ascii_letters
def sifra(text):
    new = ''
    for i in range(len(text)):
        if text[i] == 'z':
            new += 'a'
        elif text[i] == 'Z':
            new += 'A'
        else:
            a = ascii_letters.index(text[i])
            new += ascii_letters[a+1]

    return new

print(sifra(input()))



