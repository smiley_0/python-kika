def do_kolac(c, v, p):
    #print(c, v, p, end='')
    ci, vi, pi = c, v, p

    if p == 1:
        #print(c*'c' + v*'v')
        pass
    elif p == 2:
        #print('c' + v*'v' + (c-1)*'c')
        pass
    else:
        kolac = 'cvcv'
        p -= 3
        c -= 2
        v -= 2

        while p != 0:
            if kolac[-1] == 'v':
                kolac += 'c'
                c -= 1
            else:
                kolac += 'v'
                v -= 1

            p -= 1

        if c != 0:
            kolac = c*'c' + kolac
        if v != 0:
            if kolac[-1] == 'v':
                kolac += v*'v'
            else:
                kolac = kolac[:-1] + v*'v' + 'c'

        #assert c == 0, f'c is {c}'
        #assert v == 0, f'v is {v}'
        assert p == 0, f'p is {p}'
        assert len(kolac) == ci + vi

        ppp = 0
        for i in range(len(kolac) - 1):
            if kolac[i] != kolac[i+1]:
                ppp+=1

        assert ppp == pi
        assert kolac.count('c') == ci
        assert kolac.count('v') == vi
        #print(kolac)


for c in range(1, 500):
    for v in range(1, 500):
        if c%100==0 or v%100==0:
            print(c, v)
        for p in range(1, 2*min(c, v)):
            try:
                do_kolac(c, v, p)
            except Exception as e:
                print(c, v, p, e)
