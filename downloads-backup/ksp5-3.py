def pocitadlo(s, x, y):
    return s.count(x), s.count(y)

def stranovadlo(x):
    prave = {']', ')', '}'}
    if x in prave:
        return True
    else:
        return False


def otacadlo(x):
    if x == '(':
        return ')'
    elif x == ')':
        return '('
    elif x == '{':
        return '}'
    elif x == '}':
        return '{'
    elif x == '[':
        return ']'
    else:
        return '['

def urcovadlo(nieco, x, y):
    if nieco[0] > nieco[1]:
        return y
    elif nieco[0] < nieco[1]:
        return x
    else:
        return None

def overovadlo(s):
    pole = []
    chyba = 0
    try:
        for pos, znak in enumerate(s):
            if znak == '[' or znak == '{' or znak == '(':
                pole.append((znak, pos))
            elif znak == ']' and pole[-1][0] == '[':
                pole.pop()
            elif znak == ')' and pole[-1][0] == '(':
                pole.pop()
            elif znak == '}' and pole[-1][0] == '{':
                pole.pop()
            else:
                return pole[-1]
    except:
        return False

    return len(pole) == 0

for i in range(int(input())):
    vstup = input()
    kucerave = pocitadlo(vstup, '{', '}')
    hranate = pocitadlo(vstup, '[', ']')
    oble = pocitadlo(vstup, '(', ')')

    #print(kucerave, hranate, oble)

    missing = None
    if missing is None:
        missing = urcovadlo(kucerave, '{', '}')

        if missing is None:
            missing = urcovadlo(hranate, '[', ']')

            if missing is None:
                missing = urcovadlo(oble, '(', ')')

    #print(missing)
    n = []
    for i in range(len(vstup)):
        if vstup[i] == otacadlo(missing):
            n.append(i)
    p = 0
    if stranovadlo(missing):
        for i in range(n[0], len(vstup)+1):
            x = vstup[:n[0]] + vstup[n[0]:i] + missing + vstup[i:]
            #print(x)
            if overovadlo(x):
                p += 1
    else:
        for i in range(n[-1]+1):
            x = vstup[:i] + missing + vstup[i:]
            if overovadlo(x):
                p += 1

    print(overovadlo(vstup))
    print(p)
