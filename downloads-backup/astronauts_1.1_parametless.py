import matplotlib.pyplot as plt
from numpy import sin, tan, e
from scipy import optimize

f = open("data01.csv", "r")

time = []
humidity = []
temperature = []

# reading values
for line in f.readlines():
    values = line.split(",")

    # time
    time_s = values[0].split()[1][:8]
    hour = int(time_s.split(":")[0])
    min = int(time_s.split(":")[1])
    sec = int(time_s.split(":")[2])
    time.append(hour*3600 + min*60 + sec)

    # humidity
    humidity.append(float("{:.6f}".format(float(values[1]))))

    # temperature
    av_temp = (float(values[2]) + float(values[3]) + float(values[4]))/3
    temperature.append(float("{:.6f}".format(av_temp)))


# initial time set to 0
poc = time[0]
for i in range(len(time)):
    time[i] -= poc
    time[i] /= 60


# function for fitting the values
def fit_func(x, a, b, c, d, j, f, g, h, i, k):
    global e
    return a*x**(b) + d*x**(j) + f*x**g + h*x*i + k
    # return a*b**(c*x) + d*x**(j) + f*x**g + h*x*i + k


# drawing humidity values scattered
plt.subplot(211)
plt.plot(time, humidity, ".")

plt.xlabel("time / min")
plt.ylabel("humidity / %")

# calculating parameters for fitting humidity function
params, blsht = optimize.curve_fit(fit_func, time, humidity, maxfev=200000)
for i in range(10):
    print(i)
    params, blsht = optimize.curve_fit(fit_func, time, humidity, maxfev=200000, p0=params)
print(params)

# draving out the fitted function
hum_fit_y = []
for i in range(len(time)):
    hum_fit_y.append(fit_func(time[i], *params))

plt.plot(time, hum_fit_y, "r")


# drawing temperature values scattered
plt.subplot(212)
plt.plot(time, temperature, ".")

plt.xlabel("time / min")
plt.ylabel("temperature / °C")

# calculating parameters for fitting temperature function
params, blsht = optimize.curve_fit(fit_func, time, temperature, maxfev=100000,)
for i in range(10):
    print(i)
    params, blsht = optimize.curve_fit(fit_func, time, temperature, maxfev=100000, p0=params)
print(params)

# draving out the fitted function
temp_fit_y = []
for i in range(len(time)):
    temp_fit_y.append(fit_func(time[i], *params))

plt.plot(time, temp_fit_y, "r")

plt.show()
