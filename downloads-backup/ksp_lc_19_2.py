n, r = (int(i) for i in input().split())

je = [int(i) for i in range(r, n+1)]

def funkcia_na_delitele(cislo):
    delitele = []
    for i in range(1, cislo+1):
        if cislo%i == 0:
            delitele.append(i)

    return delitele

delitele = funkcia_na_delitele(r-1)

for i in range(2, r):
    if i not in delitele:
        je.append(i)

citatel = len(je)

def gcd(a, b):
    if b == 0: return a
    else:
        return gcd(b, a%b)

print(citatel//gcd(citatel, n), n//gcd(citatel, n))
