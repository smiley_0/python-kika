c, v, p = input().split()
c = int(c)
v = int(v)
p = int(p)

if p == 1:
    print(c*'c' + v*'v')
elif p == 2:
    print('c' + v*'v' + (c-1)*'c')
else:
    kolac = 'cvcv'
    p -= 3
    c -= 2
    v -= 2

    while p != 0:
        if kolac[-1] == 'v':
            kolac += 'c'
            c -= 1
        else:
            kolac += 'v'
            v -= 1

        p -= 1

    if c != 0:
        kolac = c*'c' + kolac
    if v != 0:
        if kolac[-1] == 'v':
            kolac += v*'v'
        else:
            kolac = kolac[:-1] + v*'v' + 'c'

    print(kolac)