n, q = (int(i) for i in input().split())

aplikacie = []

for i in range(q):
    udalost, x_t = (int(i) for i in input().split())

    if udalost == 1:
        aplikacie.append(x_t)
    elif udalost == 2:
        while True:
            try:
                aplikacie.remove(x_t)
            except ValueError:
                break
    else:
        aplikacie = aplikacie[x_t:]

    print(udalost, x_t)
    print(aplikacie)
    print(len(aplikacie))
    print(10*'-')
