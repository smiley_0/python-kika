Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ns/call  ns/call  name    
 58.60      4.04     4.04 38476479   104.94   104.94  median
 12.58      4.90     0.87 38476800    22.52    22.52  gen_pixel
  7.92      5.45     0.55 38476800    14.19    14.19  clip_window
  5.82      5.85     0.40 38476800    10.42    10.42  shift_window
  5.67      6.24     0.39 38476800    10.16   153.63  pixel_processing
  4.07      6.52     0.28 38476800     7.29     7.29  buffer
  2.98      6.73     0.21 38476800     5.34    37.24  system_input
  1.02      6.80     0.07                             main
  0.73      6.85     0.05 38476479     1.30     1.30  thresholding
  0.29      6.87     0.02                             print_results
  0.00      6.87     0.00      501     0.00     0.00  update_base_pos
  0.00      6.87     0.00       50     0.00     0.00  histogram_clean
  0.00      6.87     0.00       50     0.00     0.00  otsu

