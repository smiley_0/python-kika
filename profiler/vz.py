#!/usr/bin/env python3

times = {}

for x in range(1, 10):
    with open(f'gprof_{x}.txt', 'r') as f:
        lines = f.readlines()[5:-1]
        for line in lines:
            lc = line.split()
            if len(lc) == 0:
                continue

            if lc[-1] not in times.keys():
                times[lc[-1]] = []
            times[lc[-1]].append(float(lc[2]))

summ = 0
calculated = {}
for key, val in times.items():
    calculated[key] = sum(val)/len(val)
    summ += calculated[key]

sx = 0
labels = []
for k, v in sorted(calculated.items(), key=lambda x: x[1], reverse=True):
    k = k.replace('_', '\\_')
    labels.append(f'\\texttt{{{k}}}')
    vvvv = v/summ*100
    print(f'\\texttt{{{k}}} & \\SI{{{vvvv:.3f}}}{{\\percent}} \\\\')
    sx += vvvv

print()

print(f'symbolic x coords={{{", ".join(reversed(labels))}}}')

for k, v in sorted(calculated.items(), key=lambda x: x[1], reverse=True):
    k = k.replace('_', '\\_')
    labels.append(k)
    vvvv = v/summ*100
    print(f'({vvvv:.3f},\\texttt{{{k}}})')
