import pygame
import sys
pygame.init()

size = width, height = 900, 700
background = 0, 255, 0

clock = pygame.time.Clock()

screen = pygame.display.set_mode(size)

def sachovnica(n, x=0, y=0, add=1):
    p = 1
    for i in range((800//n)+add):
        for j in range((600//n)+add):
            if p == 1:
                p = 0
            else:
                pygame.draw.rect(screen, (0, 0, 255),
                    (x + i*n, y + j*n, n, n,)
                )
                p = 1

# Offsets
x, y = 0, 0

block_size = 20

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    screen.fill(background)

    delta = clock.get_time() / 1000
    x -= 50 * delta
    y -= 50 * delta

    if x <= -block_size:
        x = 0

    if y <= -block_size:
        y = 0

    sachovnica(block_size, x=round(x)+50, y=round(y)+50)

    pygame.display.flip()
    clock.tick()









