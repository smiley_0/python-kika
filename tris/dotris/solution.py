def generate_row(cols):
    return [False for _ in range(cols)]

def find_empty(matrix, col):
    """Find lowest empty cell in column `col`"""
    for row in range(len(matrix) - 1, -1, -1):
        if matrix[row][col] == True:
            return row + 1

    return 0

def print_matrix(matrix):
    for row in reversed(matrix):
        print('|', end='')
        for col in row:
            print('#' if col else ' ', end='')
        print('|')

    print('-' * (len(matrix[0]) + 2))

# Get inputs
r, c, n = (int(n) for n in input().split())

# Generate empty matrix
matrix = [generate_row(c) for _ in range(r)]
line_clears = 0

for _ in range(n):
    inp = input().split()
    col, rot = int(inp[0]), inp[1]

    row = find_empty(matrix, col)
    if rot == '_':
        # Search in next col, get highest point
        row = max(row, find_empty(matrix, col + 1))

    if row + (1 if rot == 'I' else 0) >= r:
        print(f'GAME OVER')
        break

    matrix[row][col] = True
    if rot == 'I':
        matrix[row+1][col] = True
    else:
        matrix[row][col+1] = True

    # Check for potential clears
    for rr in range(min(row+2, r-1), row-1, -1):
        if sum(matrix[rr]) == c:
            matrix.pop(rr)
            matrix.append(generate_row(c))
            line_clears += 1

    print_matrix(matrix)

print(f'Score:\t{line_clears}\n')
print_matrix(matrix)
