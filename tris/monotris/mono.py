def generate_row(cols):
    return [False for _ in range(cols)]

def find_empty(matrix, col):
    """Find lowest empty cell in column `col`"""
    for row in range(len(matrix) - 1, -1, -1):
        if matrix[row][col] == True:
            return row + 1

    return 0

def print_matrix(matrix):
    for row in reversed(matrix):
        print('|', end='')
        for col in row:
            print('#' if col else ' ', end='')
        print('|')

    print('-' * (len(matrix[0]) + 2))

# Get inputs
r, c, n = (int(n) for n in input().split())

# Generate empty matrix
matrix = [generate_row(c) for _ in range(r)]
line_clears = 0

for _ in range(n):
    col = int(input())
    row = find_empty(matrix, col)
    if row >= r:
        print(f'GAME OVER')
        break

    matrix[row][col] = True

    # Check 0-th row for potential clears
    if sum(matrix[0]) == c:
        matrix.pop(0)
        matrix.append(generate_row(c))
        line_clears += 1

print(f'Score:\t{line_clears}\n')
print_matrix(matrix)
