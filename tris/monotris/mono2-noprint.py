# Get inputs
r, c, n = (int(n) for n in input().split())

# Generate empty matrix
matrix = [0 for _ in range(c)]
line_clears = 0

for _ in range(n):
    col = int(input())
    matrix[col] += 1
    if matrix[col] > r:
        print(f'GAME OVER')
        break

    # Check 0-th row for potential clears
    mm = min(matrix)
    if mm > 0:
        line_clears += mm
        # Can be done with map() too
        for i in range(len(matrix)):
            matrix[i] -= mm

print(f'Score:\t{line_clears}\n')
