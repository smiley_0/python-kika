r, c, n = (int(n) for n in input().split())

vsetko = []
for i in range(c):
    vsetko.append(0)

score = 0
for i in range(n):
    a = int(input())
    vsetko[a] += 1
    if vsetko[a] > r:
        print('game over')
        break
    a = min(vsetko)
    score += a
    for i in range(c):
        vsetko[i] -= a

print(score)
