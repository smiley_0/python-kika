def print_matrix(matrix, rows):
    for row in range(rows, 0, -1):
        print('|', end='')
        for val in matrix:
            print('#' if val >= row else ' ', end='')
        print('|')

    print('-' * (len(matrix) + 2))

# Get inputs
r, c, n = (int(n) for n in input().split())

# Generate empty matrix
matrix = [0 for _ in range(c)]
line_clears = 0

for _ in range(n):
    col = int(input())
    matrix[col] += 1
    if matrix[col] > r:
        print(f'GAME OVER')
        break

    # Check 0-th row for potential clears
    mm = min(matrix)
    if mm > 0:
        line_clears += mm
        # Can be done with map() too
        for i in range(len(matrix)):
            matrix[i] -= mm

    print_matrix(matrix, r)

print(f'Score:\t{line_clears}\n')
print_matrix(matrix, r)
