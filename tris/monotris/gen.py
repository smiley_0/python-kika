from random import randint, shuffle

r, c = randint(1, 20), randint(1, 20)

toclear = randint(1, r*5)
elements = list(range(c)) * toclear
for _ in range(randint(1, r*2)):
    elements.append(randint(0, c-1))
shuffle(elements)

print(r, c, len(elements))
for e in elements:
    print(e)
