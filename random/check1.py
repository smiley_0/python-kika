import tkinter as tk

ops = {
    'Parne': lambda x: 'Parne' if x % 2 == 0 else 'Neparne',
    'Dvojciferne': lambda x: 'Dvojciferne' if len(str(x)) == 2 else 'Nie je dvojciferne',
    'Delitelne 7': lambda x: 'Delitelne 7' if x % 7 == 0 else 'Nedelitelne 7',
}

def doop(*x):
    global num, v, l
    try:
        txt = []
        for k, val in v.items():
            if val.get() == 1:
                txt.append(ops[k](num.get()))
        l.config(text='\n'.join(txt))
    except tk.TclError as e:
        l.config(text='NaN')

a = tk.Tk()
a.geometry('200x200')

num = tk.IntVar()
v = {}

en = tk.Spinbox(a, textvariable=num, from_=-100000, to=100000)
en.pack()

for k, val in ops.items():
    vx = tk.IntVar()
    b = tk.Checkbutton(a, text=k, variable=vx, command=doop)
    b.pack()
    v[k] = vx

l = tk.Label(a)
l.pack()

num.set(0)
num.trace_add("write", doop)

a.mainloop()
