import tkinter as tk

ops = {
    'Male pismena': lambda x: sum(map(str.islower, x)),
    'Velke pismena': lambda x: sum(map(str.isupper, x)),
    'Cisla': lambda x: sum(map(str.isnumeric, x)),
}

def doop(*x):
    global txtv, v, l
    try:
        txt = []
        for k, val in v.items():
            if val.get() == 1:
                txt.append(f'{k}: {ops[k](txtv.get())}')
        l.config(text='\n'.join(txt))
    except tk.TclError as e:
        l.config(text='NaN')

a = tk.Tk()
a.geometry('200x200')

txtv = tk.StringVar()
v = {}

en = tk.Entry(a, textvariable=txtv)
en.pack()

for k, val in ops.items():
    vx = tk.IntVar()
    b = tk.Checkbutton(a, text=k, variable=vx, command=doop)
    b.pack()
    v[k] = vx

l = tk.Label(a)
l.pack()

txtv.set('')
txtv.trace_add('write', doop)

a.mainloop()
