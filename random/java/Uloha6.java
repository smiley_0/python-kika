import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Uloha6
 */
public class Uloha6 {

    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("vstup.txt");
            BufferedReader reader = new BufferedReader(fin);
            FileWriter fout = new FileWriter("u6-vystup.txt");
            BufferedWriter writer = new BufferedWriter(fout);

            String row;
            while ((row = reader.readLine()) != null) {
                if (row.trim().length() > 0) {
                    writer.write(row + "\n");
                }
            }

            reader.close();
            writer.close();

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
