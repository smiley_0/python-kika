import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Uloha5
 */
public class Uloha5 {

    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("vstup.txt");
            BufferedReader reader = new BufferedReader(fin);

            FileWriter fout1 = new FileWriter("u5-vystup-1.txt");
            BufferedWriter writer1 = new BufferedWriter(fout1);
            FileWriter fout2 = new FileWriter("u5-vystup-2.txt");
            BufferedWriter writer2 = new BufferedWriter(fout2);


            String row;
            boolean swap = true;
            while ((row = reader.readLine()) != null) {
                StringBuilder s1 = new StringBuilder(row);
                StringBuilder s2 = new StringBuilder(row);

                for (int i = 0; i < row.length(); i++) {
                    if (swap) {
                        s1.setCharAt(i, ' ');
                    } else {
                        s2.setCharAt(i, ' ');
                    }
                    swap = !swap;
                }

                writer1.write(s1.toString() + "\n");
                writer2.write(s2.toString() + "\n");
            }

            reader.close();
            writer1.close();
            writer2.close();

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
