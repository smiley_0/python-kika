import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Uloha4
 */
public class Uloha4 {

    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("vstup.txt");
            BufferedReader reader = new BufferedReader(fin);
            FileWriter fout = new FileWriter("u4-vystup.txt");
            BufferedWriter writer = new BufferedWriter(fout);

            String row;
            while ((row = reader.readLine()) != null) {
                String last = row.substring(row.length() - 1);

                if (last.equals(".") || last.equals(",")) {
                    row = row.substring(0, row.length() - 1);
                } else {
                    last = "";
                }

                writer.write(new StringBuilder(row).reverse().toString() + last + "\n");
            }

            reader.close();
            writer.close();

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
