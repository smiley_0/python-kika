import tkinter as tk

a = tk.Tk()
a.geometry('200x200')

m = [None, 'januar', 'februar', 'marec', 'april', 'maj', 'jun', 'jul', 'august', 'september', 'oktober', 'november', 'december']

def onclick():
    l.config(text=f'Dnes je {day.get()}. {m[int(mon.get())]}.')


day = tk.Spinbox(a, from_=1, to=31)
mon = tk.Spinbox(a, from_=1, to=12)
b = tk.Button(a, text='datum', command=onclick)
l = tk.Label(a, text='Dnes je:')

day.pack()
mon.pack()
b.pack()
l.pack()

a.mainloop()
