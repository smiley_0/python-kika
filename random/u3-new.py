import tkinter as tk # skratka pre tkinter
from random import randrange as rnd # skratka pre randrange

#rozmery canvasu
w = 500
h = 500

okno = tk.Tk()
canvas = tk.Canvas(okno, width=w, height=h, bg="green")
canvas.pack()

# funkcia na vycistenie canvasu
def zmaz():
    canvas.delete("all")

# funkcia na vykreslovanie textu
def pis():
    # suradnice vygenerovaneho kruhu
    x = rnd(w)
    y = rnd(h)

    # pocet textov v kruhu
    pocet = int(p.get())
    # farba textu
    farba = f.get()
    # text
    text = txt.get()
    # uhol sklonu textu v kruhu
    kruh = 360.0 / pocet

    for i in range(pocet):
        # vytvorenie textu
        canvas.create_text(
            x, y,
            anchor='w',
            angle=i*kruh,
            text=f'   {text}',
            fill=farba,
        )

p = tk.Entry(okno)
p.insert(0, '5')
f = tk.Entry(okno)
f.insert(0, 'red')
txt = tk.Entry(okno)
txt.insert(0, 'text')

pis_tlac = tk.Button(okno, text="pis", command=pis)
zmaz_tlac = tk.Button(okno, text="zmaz", command=zmaz)

p.pack()
f.pack()
txt.pack()
pis_tlac.pack()
zmaz_tlac.pack()
okno.mainloop()
