x = int(input())
q = int(input())
t = [int(x) for x in input().split()]

bankovky = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, x]
bankovky.sort(reverse=True)

def zaplat(suma, pocet=0):
    print('zaplat suma =', suma, 'pocet =', pocet)
    if suma == 0:
        return pocet

    najlepsie = 1000000000
    naj_bankovka = x
    for b in bankovky:
        if suma - b >= 0:
            print('\tskusam', b)
            xxx = zaplat(suma - b, pocet + 1)
            if xxx < najlepsie:
                najlepsie = xxx
                naj_bankovka = b

    print('\tnajlepsie to dam s', naj_bankovka, najlepsie)
    return najlepsie

for ti in t:
    print(zaplat(ti))
