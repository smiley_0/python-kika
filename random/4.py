n = int(input())
ballz = input()
najviac = 0

for zaciatok in range(n):
    pocty = {}
    neparne = 0

    for koniec in range(zaciatok, n):
        if ballz[koniec] not in pocty:
            pocty[ballz[koniec]] = 1
        else:
            pocty[ballz[koniec]] += 1

        if pocty[ballz[koniec]] % 2 != 0:
            neparne += 1
        else:
            neparne -= 1

        assert neparne == sum(n % 2 != 0 for n in pocty.values())

        length = koniec - zaciatok + 1

        if neparne <= 1 and najviac < length:
            najviac = length


        # print(pocty)
print(najviac)
