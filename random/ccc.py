n = int(input())

i = n
while True:
    s = sum([int(x) for x in str(i)])

    if s == n:
        print(i)
        break

    i += n
