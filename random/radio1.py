import tkinter as tk

farby = {
    'Cierna': 'black',
    'Modra': 'blue',
    'Cervena': 'red',
    'Zelena': 'green',
}

a = tk.Tk()
a.geometry('200x200')

def color():
    global v
    a.configure(background=v.get())

frm = tk.Frame(a)
v = tk.StringVar()
for k, vx in farby.items():
    b = tk.Radiobutton(frm, text=k, variable=v, command=color, value=vx)
    b.pack()

frm.pack(side=tk.LEFT)

a.mainloop()
