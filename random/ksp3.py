n, m, f = [int(i) for i in input().split()]
tuba = [int(i) for i in input().split()]
prva_kopia = [0 for i in range(f + 1)]

for i in range(m):
    a, f = [int(i) for i in input().split()]
    prva_kopia[f] = a

l = 0

def nieco(poziadavky_original):
    poziadavky = poziadavky_original[:]
    dokopy = sum(poziadavky)
    r = n
    while dokopy > 0:
        r -= 1
        if poziadavky[tuba[r]] > 0:
            poziadavky[tuba[r]] -= 1
            dokopy -= 1
    return n - r

minimum = l + nieco(prva_kopia)

while l < n:
    if prva_kopia[tuba[l]] > 0:
        prva_kopia[tuba[l]] -= 1

        vysledok = 1 + l + nieco(prva_kopia)
        if vysledok < minimum:
            minimum = vysledok
    l += 1

print(minimum)
