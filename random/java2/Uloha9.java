import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Uloha9 {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            FileReader fin = new FileReader("stanice.txt");
            BufferedReader reader = new BufferedReader(fin);

            System.out.println("Typ oblacnosti? (JJ/PO/PJ/OO)");
            String oblacnost = in.next().trim().toUpperCase();

            Double min_temp = Double.POSITIVE_INFINITY;
            Double max_temp = Double.NEGATIVE_INFINITY;
            List<String> min_temp_stat = new ArrayList<String>();
            List<String> max_temp_stat = new ArrayList<String>();
            int obl_count = 0;

            String row;
            while ((row = reader.readLine()) != null) {
                String[] r = row.split(" ");
                Double temp = Double.parseDouble(r[3]);

                if (temp.compareTo(min_temp) == 0) {
                    min_temp_stat.add(r[0]);
                } else if (temp < min_temp) {
                    min_temp = temp;
                    min_temp_stat.clear();
                    min_temp_stat.add(r[0]);
                }

                if (temp.compareTo(max_temp) == 0) {
                    max_temp_stat.add(r[0]);
                } else if (temp > max_temp) {
                    max_temp = temp;
                    max_temp_stat.clear();
                    max_temp_stat.add(r[0]);
                }

                if (r[4].equals(oblacnost)) {
                    obl_count++;
                }
            }

            System.out.println(String.format("Najnizsia teplota: %.2f C, stanice: %s", min_temp, min_temp_stat));
            System.out.println(String.format("Najvyssia teplota: %.2f C, stanice: %s", max_temp, max_temp_stat));
            System.out.println(String.format("Pocet stanic s oblacnostou %s: %d", oblacnost, obl_count));

            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
