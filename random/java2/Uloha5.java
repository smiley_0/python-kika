import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Uloha5 {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            FileReader fin = new FileReader("hobbit.txt");
            BufferedReader reader = new BufferedReader(fin);

            System.out.print("k = ");
            int k = in.nextInt();
            int rows = 0, sentences = 0, words = 0;
            String longest = "";
            List<String> k_words = new ArrayList<String>();

            String row;
            while ((row = reader.readLine()) != null) {
                rows++;

                for (String w : row.split(" ")) {
                    words++;
                    if (w.contains(".")) {
                        sentences++;
                    }

                    w = w.replaceAll("[.,]", "");

                    if (w.length() == k) {
                        k_words.add(w);
                    }

                    if (w.length() > longest.length()) {
                        longest = w;
                    }
                }
            }

            System.out.println(String.format("Pocet riadkov: %d", rows));
            System.out.println(String.format("Pocet viet: %d", sentences));
            System.out.println(String.format("Pocet slov: %d", words));
            System.out.println(String.format("Najdlhsie slovo: %s (%d)", longest, longest.length()));
            System.out.println(String.format("Slova dlzky %d:", k));
            for (String w : k_words) {
                System.out.println("\t" + w);
            }


            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
