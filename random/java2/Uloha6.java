import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Uloha6 {
    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("rodnecisla.txt");
            BufferedReader reader = new BufferedReader(fin);

            int total = 0, men = 0;
            String row;
            while ((row = reader.readLine()) != null) {
                total++;
                char x = row.split(" ")[2].charAt(2);
                if (x == '0' || x == '1') {
                    men++;
                }
            }

            System.out.println(String.format("Celkom: %d, Muzov: %d, Zien: %d", total, men, total-men));

            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
