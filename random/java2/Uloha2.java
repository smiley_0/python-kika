import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Uloha2 {
    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("vysvedcenie.txt");
            BufferedReader reader = new BufferedReader(fin);

            String[] znamky = reader.readLine().split(" ");
            int sum = 0;
            int max_zn = 1;
            for (String znamka : znamky) {
                int z = Integer.parseInt(znamka);
                sum += z;
                max_zn = (z > max_zn) ? z : max_zn;
            }

            Double avg = (double)sum / znamky.length;

            System.out.println(String.format("Priemerna znamka: %.2f", avg));

            System.out.print("Celkovy prospech: ");
            if (max_zn <= 2 && avg <= 1.5) {
                System.out.println("PV");
            } else if (max_zn <= 3 && avg <= 2.0) {
                System.out.println("PVD");
            } else if (max_zn <= 4) {
                System.out.println("P");
            } else {
                System.out.println("N");
            }

            System.out.println(String.format("Pocet znamok: %d", znamky.length));

            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
