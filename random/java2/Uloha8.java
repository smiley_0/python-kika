import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Uloha8 {
    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("park.txt");
            BufferedReader reader = new BufferedReader(fin);

            List<String> vchody = new ArrayList<String>();
            int trava_plocha = 0;
            int hnojivo = 0;

            reader.readLine();  // prvy riadok
            String row;
            for (int r = 0; (row = reader.readLine()) != null; r++) {
                System.out.println(row);
                for (int c = 0; c < row.length(); c++) {
                    switch (row.charAt(c)) {
                        case 'T':
                            trava_plocha++;
                            hnojivo += 100;
                            break;

                        case 'K':
                            hnojivo += 20;
                            break;

                        case 'S':
                            hnojivo += 500;
                            break;

                        case 'V':
                            vchody.add(String.format("[%d, %d]", r, c));
                            break;

                        default:
                            break;
                    }
                }
            }

            System.out.println("Suradnice vchodov: ");
            for (String v : vchody) {
                System.out.println("\t" + v);
            }
            System.out.println(String.format("Travnik zabera %d m2.", trava_plocha));
            System.out.println(String.format("Pouzitych %d g hnojiva", hnojivo));

            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
