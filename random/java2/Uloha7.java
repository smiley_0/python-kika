import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Uloha7 {

    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("tance.txt");
            BufferedReader reader = new BufferedReader(fin);
            FileWriter sta_fout = new FileWriter("standard.txt");
            BufferedWriter sta_writer = new BufferedWriter(sta_fout);
            FileWriter lat_fout = new FileWriter("latino.txt");
            BufferedWriter lat_writer = new BufferedWriter(lat_fout);

            int sk_total = 0, cz_total = 0;
            int sta_best_score = 0, lat_best_score = 0;
            String sta_best = "", lat_best = "";

            String row;
            while ((row = reader.readLine()) != null) {
                String[] v = row.split(" ");
                int score = Integer.parseInt(v[3]);

                if (v[1].equals("STA")) {
                    sta_writer.write(row + "\n");
                    if (score > sta_best_score) {
                        sta_best_score = score;
                        sta_best = v[0] + " " + v[2];
                    }
                } else if (v[1].equals("LAT")) {
                    lat_writer.write(row + "\n");
                    if (score > lat_best_score) {
                        lat_best_score = score;
                        lat_best = v[0] + " " + v[2];
                    }
                }

                if (v[2].equals("SK")) {
                    sk_total += score;
                } else if (v[2].equals("CZ")) {
                    cz_total += score;
                }
            }

            System.out.println(String.format("Uspesnejsia krajina: %s", (sk_total == cz_total) ? "Remiza" : ((sk_total > cz_total) ? "SK" : "CZ")));
            System.out.println(String.format("Najlepsi standardny par: %s (%d)", sta_best, sta_best_score));
            System.out.println(String.format("Najlepsi latino par: %s (%d)", lat_best, lat_best_score));

            reader.close();
            sta_writer.close();
            lat_writer.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
