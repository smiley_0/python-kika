import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Uloha4 {
    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("hobbit.txt");
            BufferedReader reader = new BufferedReader(fin);

            List<String> l = new ArrayList<String>();

            String row;
            while ((row = reader.readLine()) != null) {
                l.add(row);
            }

            for (int i = l.size() - 1; i >= 0; i--) {
                System.out.println(l.get(i));
            }

            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
