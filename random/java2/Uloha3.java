import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Uloha3 {
    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("pali.txt");
            BufferedReader reader = new BufferedReader(fin);

            String row;
            int pali = 0;
            while ((row = reader.readLine()) != null) {
                row = row.replaceAll(" ", "");
                String rev = new StringBuilder(row).reverse().toString();

                if (row.equalsIgnoreCase(rev)) {
                    pali++;
                }
            }

            System.out.println(pali);
            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
