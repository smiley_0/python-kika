import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Uloha1
 */
public class Uloha1 {

    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("ziaci.txt");
            BufferedReader reader = new BufferedReader(fin);
            FileWriter fout = new FileWriter("u1-vystup.txt");
            BufferedWriter writer = new BufferedWriter(fout);

            List<String> ziak_best = new ArrayList<String>();
            List<String> ziak_worst = new ArrayList<String>();
            Double best_avg = Double.POSITIVE_INFINITY;
            Double worst_avg = Double.NEGATIVE_INFINITY;

            int tot_sum = 0;
            int grd_cnt = 0;

            String row;
            String name = "";
            boolean row_name = true;
            while ((row = reader.readLine()) != null) {
                if (row_name) {
                    name = row;
                    writer.write(row + '\n');
                } else {
                    String[] grades = row.split(" ");
                    int sum = 0;
                    for (String grade : grades) {
                        int grd = Integer.parseInt(grade);
                        sum += grd;
                        tot_sum += grd;
                        grd_cnt++;
                    }
                    Double avg = (double)sum / grades.length;
                    writer.write(String.format("%.2f\n", avg));

                    if (avg < best_avg) {
                        ziak_best.clear();
                        ziak_best.add(name);
                        best_avg = avg;
                    } else if (Double.compare(avg, best_avg) == 0) {
                        ziak_best.add(name);
                    }

                    if (avg > worst_avg) {
                        ziak_worst.clear();
                        ziak_worst.add(name);
                        worst_avg = avg;
                    } else if (Double.compare(avg, worst_avg) == 0) {
                        ziak_worst.add(name);
                    }
                }

                row_name = !row_name;
            }

            System.out.println(ziak_best.size() > 1 ? "Najlepsi ziaci:" : "Najlepsi ziak:");
            for (String ziak : ziak_best) {
                System.out.println(String.format("\t%s\t(%.2f)", ziak, best_avg));
            }

            System.out.println(ziak_best.size() > 1 ? "Najhorsi ziaci:" : "Najhorsi ziak:");
            for (String ziak : ziak_worst) {
                System.out.println(String.format("\t%s\t(%.2f)", ziak, worst_avg));
            }

            System.out.println(String.format("Priemer triedy: %.2f", (double)tot_sum / grd_cnt));

            reader.close();
            writer.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
