import tkinter as tk

ops = {
    'Parne': lambda x: 'Parne' if x % 2 == 0 else 'Neparne',
    'Dvojciferne': lambda x: 'Dvojciferne' if len(str(x)) == 2 else 'Nie je dvojciferne',
    'Delitelne 7': lambda x: 'Delitelne 7' if x % 7 == 0 else 'Nedelitelne 7',
}

def doop(*x):
    global num, v, l
    try:
        l.config(text=ops[v.get()](num.get()))
    except tk.TclError as e:
        l.config(text='NaN')

a = tk.Tk()
a.geometry('200x200')

num = tk.IntVar()
num.trace_add("write", doop)
v = tk.StringVar()
v.set(list(ops.keys())[0])

en = tk.Entry(a, textvariable=num)
en.pack()

for k, val in ops.items():
    b = tk.Radiobutton(a, text=k, variable=v, command=doop, value=k)
    b.pack()

l = tk.Label(a)
l.pack()

a.mainloop()
