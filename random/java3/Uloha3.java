import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Uloha3 {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            FileReader fin = new FileReader("knihovnik.txt");
            BufferedReader reader = new BufferedReader(fin);

            System.out.print("Sirka policky? ");
            Double sirka = in.nextDouble();
            Double sum = 0.0;
            int police = 1;

            String row;
            while ((row = reader.readLine()) != null) {
                Double k = Double.parseDouble(row);
                sum += k;
                if (sum >= sirka) {
                    sum = k;
                    police++;
                }
            }

            if (police == 1) {
                System.out.println("Vsetky knihy sa zmestia do policky.");
            } else {
                System.out.println(String.format("Pocet policiek na ulozenie vsetkych knih: %d.", police));
            }

            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
