import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Uloha5 {
    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("matematika.txt");
            BufferedReader reader = new BufferedReader(fin);

            FileWriter add_out = new FileWriter("petko.txt");
            BufferedWriter add_writer = new BufferedWriter(add_out);

            FileWriter mul_out = new FileWriter("palko.txt");
            BufferedWriter mul_writer = new BufferedWriter(mul_out);

            int add = 0, mul = 0;
            String row;
            while ((row = reader.readLine()) != null) {
                String r[] = row.split(" ");
                int o1 = Integer.parseInt(r[1]), o2 = Integer.parseInt(r[2]);

                if (r[0].equals("+")) {
                    add_writer.write(String.format("+ %d %d %d\n", o1, o2, o1+o2));
                    add++;
                } else if (r[0].equals("*")) {
                    mul_writer.write(String.format("* %d %d %d\n", o1, o2, o1*o2));
                    mul++;
                }
            }

            if (add == mul) {
                System.out.println("Vypocitali rovnako vela.");
            } else if (add > mul) {
                System.out.println("Petko vypocital viac.");
            } else {
                System.out.println("Palko vypocital viac.");
            }

            reader.close();
            add_writer.close();
            mul_writer.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
