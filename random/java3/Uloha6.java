import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Uloha6 {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            FileReader fin = new FileReader("sifra.txt");
            BufferedReader reader = new BufferedReader(fin);
            FileWriter fout = new FileWriter("u6-vystup.txt");
            BufferedWriter writer = new BufferedWriter(fout);

            System.out.print("Kluc = ");
            int key = in.nextInt();

            int c;
            while ((c = reader.read()) != -1) {
                if (c >= 'a' && c <= 'z') {
                    c = ((c - 'a' + key) % ('z' - 'a' + 1)) + 'a';
                } else if (c >= 'A' && c <= 'z') {
                    c = ((c - 'A' + key) % ('Z' - 'A' + 1)) + 'A';
                }

                writer.write(c);
            }


            reader.close();
            writer.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
