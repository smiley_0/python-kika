import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Uloha2 {
    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("zbierka.txt");
            BufferedReader reader = new BufferedReader(fin);

            int cnt = 0;
            Double total = 0.0;
            Double max = 0.0;

            String row;
            while ((row = reader.readLine()) != null) {
                cnt++;
                int sec = Integer.parseInt(row);
                Double d = sec * 0.15;
                total += d;
                max = Double.max(max, d);
            }

            System.out.println(String.format("Pocet prispevkov: %d\nVyzbierane %.2f\nNajvyssi prispevok: %.2f", cnt, total, max));

            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
