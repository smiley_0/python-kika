import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Uloha1 {
    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("znamky3.txt");
            BufferedReader reader = new BufferedReader(fin);

            String row = reader.readLine();
            String[] zn = row.split(" ");

            int s = 0;
            for (String string : zn) {
                s += Integer.parseInt(string);
            }

            System.out.println(String.format("Pocet znamok: %d\nPriemer: %f", zn.length, (double)s/zn.length));

            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
