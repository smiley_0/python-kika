import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Uloha7 {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            FileReader fin = new FileReader("sladkosti.txt");
            BufferedReader reader = new BufferedReader(fin);

            System.out.print("Pocet deti = ");
            int d = in.nextInt();

            String nazov = "";
            int pocet = 0;
            int druhy = 0;
            int pocetNaDieta = 0;

            int maxdruh = 0;
            String maxNazov = "";

            System.out.println("Vybrane sladkosti:");

            String row;
            boolean readNazov = true;
            while ((row = reader.readLine()) != null) {
                if (readNazov) {
                    nazov = row;
                } else {
                    pocet = Integer.parseInt(row);

                    if (pocet % d == 0) {
                        System.out.println("  " + nazov);
                        int cnt = pocet / d;

                        druhy++;
                        pocetNaDieta += cnt;

                        if (cnt > maxdruh) {
                            maxdruh = cnt;
                            maxNazov = nazov;
                        }
                    }
                }

                readNazov = !readNazov;
            }

            System.out.println(String.format("Najviac sladkosti kazde dieta dostane z balicka %s (%s).\nKazde dieta dostane %d druhov sladkosti, celkovo %d sladkosti.", maxNazov, maxdruh, druhy, pocetNaDieta));

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
