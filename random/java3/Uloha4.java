import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Uloha4 {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            FileReader fin = new FileReader("uroky.txt");
            BufferedReader reader = new BufferedReader(fin);

            Double maxUrok = 0.0;

            String row;
            while ((row = reader.readLine()) != null) {
                maxUrok = Double.max(Double.parseDouble(row), maxUrok);
            }

            System.out.println(String.format("Najvyhodnejsia urokova miera %.2f%%", maxUrok));

            System.out.print("Pociatocny vklad = ");
            Double vklad = in.nextDouble();
            int roky = 0;
            while (vklad < 1000000.0) {
                vklad = vklad * (1.0 + maxUrok/100);
                roky++;
                System.out.println(String.format("Rok %d\t%.3f", roky, vklad));
            }

            System.out.println(String.format("Milionari za %d rokov", roky));

            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
