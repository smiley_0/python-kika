import java.util.Scanner;

public class Uloha4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Reklama = ");
        String reklama = in.nextLine();
        int[] cnt = countLower(reklama);
        printArray(cnt);
    }

    private static int[] countLower(String s) {
        int[] cnt = new int[26];
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= 'a' && c <= 'z') {
                cnt[c - 'a']++;
            }
        }

        return cnt;
    }

    private static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                System.out.println(String.format("%c : %d", i+'a', arr[i]));
            }
        }
    }
}
