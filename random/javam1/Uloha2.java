import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Uloha2 {
    public static void main(String[] args) {
        int arr[] = genArray(30, 10, 30);
        printArray(arr);
        System.out.println(String.format("Priemerna teplota: %fC", arrAvg(arr)));

        List<Integer> mdi = minDiff(arr);
        System.out.println(String.format("Minimalny rozdiel od priemeru v dnoch: %s (%dC)", mdi, arr[mdi.get(0)]));
    }

    private static int[] genArray(int n, int min, int max) {
        Random r = new Random();
        int a[] = new int[n];

        for (int i = 0; i < a.length; i++) {
            a[i] = r.nextInt((max - min) + 1) + min;
        }

        return a;
    }

    private static void printArray(int[] arr) {
        for (int i : arr) {
            System.out.print(i + "C ");
        }
        System.out.println();
    }

    private static int arrSum(int[] arr) {
        int s = 0;
        for (int i : arr) {
            s += i;
        }
        return s;
    }

    private static double arrAvg(int[] arr) {
        return arrSum(arr) / (double)arr.length;
    }

    private static List<Integer> minDiff(int[] arr) {
        List<Integer> n = new ArrayList<Integer>();

        double avg = arrAvg(arr);
        double md = Double.MAX_VALUE;

        for (int i = 0; i < arr.length; i++) {
            double v = Math.abs(arr[i] - avg);
            int d = Double.compare(v, md);
            if (d < 0) {
                md = v;
                n.clear();
                n.add(i);
            } else if (d == 0) {
                n.add(i);
            }
        }

        return n;
    }
}
