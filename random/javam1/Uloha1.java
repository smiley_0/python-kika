import java.util.Random;

public class Uloha1 {
    public static void main(String[] args) {
        int arr[] = genArray(42);
        printArray(arr);
        System.out.println(String.format("Max prvok: %d", arrMax(arr)));
        System.out.println(String.format("Min prvok: %d", arrMin(arr)));
        System.out.println(String.format("Priemer: %f", arrAvg(arr)));
        System.out.println(String.format("Pocet podpriemernych prvkov: %d", countUnderAvg(arr)));
    }

    private static int[] genArray(int n) {
        Random r = new Random();
        int a[] = new int[n];

        for (int i = 0; i < a.length; i++) {
            a[i] = r.nextInt(100);
        }

        return a;
    }

    private static void printArray(int[] arr) {
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    private static int arrMax(int[] arr) {
        int max = Integer.MIN_VALUE;
        for (int i : arr) {
            max = Integer.max(max, i);
        }
        return max;
    }

    private static int arrMin(int[] arr) {
        int min = Integer.MAX_VALUE;
        for (int i : arr) {
            min = Integer.min(min, i);
        }
        return min;
    }

    private static int arrSum(int[] arr) {
        int s = 0;
        for (int i : arr) {
            s += i;
        }
        return s;
    }

    private static double arrAvg(int[] arr) {
        return arrSum(arr) / (double)arr.length;
    }

    private static int countUnderAvg(int[] arr) {
        double avg = arrAvg(arr);
        int c = 0;
        for (int i : arr) {
            if (i < avg) {
                c++;
            }
        }
        return c;
    }
}
