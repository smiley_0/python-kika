import java.util.Scanner;

public class Uloha6 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("n = ");
        int n = in.nextInt();
        System.out.println(fib(n, 0, 1));
    }

    private static int fib(int n, int l, int h) {
        if (n < 2) {
            return h;
        }
        System.out.print(h + " ");
        return fib(n - 1, h, h + l);
    }
}
