import java.util.Random;

public class Uloha3 {
    public static void main(String[] args) {
        Random r = new Random();
        int n = r.nextInt(500);
        int d = r.nextInt(500);
        if (r.nextBoolean()) {
            n *= -1;
        }
        System.out.print(String.format("Vygenerovany zlomok: %d/%d\nZakladny tvar: ", n, d));
        printFrac(n, d);
    }

    private static int gcd(int a, int b) {
        return b==0 ? a : gcd(b, a%b);
    }

    private static void printFrac(int n, int d) {
        int g = gcd(n, d);
        if (g < 0) {
            g *= -1;
        }
        System.out.println(String.format("%d/%d", n/g, d/g));
    }
}
