import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Uloha7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("n = ");
        int n = in.nextInt();
        boolean prime = isPrime(n);
        if (prime) {
            System.out.println("Je prvocislo");
        } else {
            System.out.print("Nie je prvocislo\nRozklad: ");
            System.out.println(factorize(n));
        }
    }

    private static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }

        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    private static List<Integer> factorize(int n) {
        List<Integer> l = new ArrayList<Integer>();
        int f = 2;
        while (n > 1) {
            if (n % f == 0) {
                l.add(f);
                n /= f;
            } else {
                f++;
            }
        }

        return l;
    }
}
