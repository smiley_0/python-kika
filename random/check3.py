import tkinter as tk

a = tk.Tk()
a.geometry('300x50')

def cmd():
    global v, l
    l.config(text=f'Suma: {sum(x.get() for x in v)}$')

v = []

for k in [100, 200, 300, 400]:
    vx = tk.IntVar()
    b = tk.Checkbutton(a, text=f'{k}$', variable=vx, onvalue=k, offvalue=0, command=cmd)
    b.pack(side=tk.LEFT)
    v.append(vx)

l = tk.Label(a)
l.pack(side=tk.LEFT)

cmd()
a.mainloop()
