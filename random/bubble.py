pole = [7, 3, 42, 1, 2, 4, 2]

while True:
    changes = 0
    for i in range(len(pole)-1):
        if pole[i] > pole[i+1]:
            pole[i+1], pole[i] = pole[i], pole[i+1]
            changes += 1

    if changes == 0:
        break

print(pole)
