import tkinter as tk

a = tk.Tk()
a.geometry('200x200')

def color():
    a.configure(background=f'#{int(r.get()):02x}{int(g.get()):02x}{int(b.get()):02x}')

r = tk.Spinbox(a, from_=0, to=255)
g = tk.Spinbox(a, from_=0, to=255)
b = tk.Spinbox(a, from_=0, to=255)

button = tk.Button(a, text='do', command=color)

for i in [r, g, b, button]:
    i.pack()

a.mainloop()
