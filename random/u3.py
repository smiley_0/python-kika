import tkinter as tk
from random import randrange as rnd

W = H = 500

a = tk.Tk()
canvas = tk.Canvas(a, width=W, height=H, bg="aquamarine")

canvas.pack()

def zmaz():
    canvas.delete("all")

def pis():
    x, y, offset = rnd(W), rnd(H), rnd(360)

    pocet = int(p.get())
    farba = color.get()
    txt = text.get()

    inc = 360.0 / pocet

    for i in range(pocet):
        canvas.create_text(
            x, y,
            anchor='w',
            angle=offset+i*inc,
            text=f'   {txt}',
            fill=farba,
            font=('Comic Sans MS', 14)
        )

p = tk.Entry(a)
p.insert(0, '5')
color = tk.Entry(a)
color.insert(0, 'red')
text = tk.Entry(a)
text.insert(0, 'text')

pis = tk.Button(a, text="pis", command=pis)
zmaz = tk.Button(a, text="zmaz", command=zmaz)

p.pack()
color.pack()
text.pack()
pis.pack(side=tk.LEFT)
zmaz.pack(side=tk.LEFT)
a.mainloop()
