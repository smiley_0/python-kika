import tkinter as tk

BUTTONS = 3

a = tk.Tk()
a.geometry('200x200')

p = tk.IntVar()
pp = [tk.IntVar() for x in range(BUTTONS)]

def on_click(var):
    global p
    p.set(p.get() + 1)
    var.set(var.get() + 1)

for i, var in enumerate(pp):
    b = tk.Button(a, text=f'{i+1}', command=lambda var=var: on_click(var))
    b.grid(row=i, column=0)

    ll = tk.Label(a, textvariable=var)
    ll.grid(row=i, column=1)

textik = tk.Label(a, text='Sum')
textik.grid(row=BUTTONS + 1, column=0)

l = tk.Label(a, textvariable=p)
l.grid(row=BUTTONS + 1, column=1)

a.mainloop()
