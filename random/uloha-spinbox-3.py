
from tkinter import *
okno = Tk()
okno.geometry("400x400")

def farba():
    okno.configure(background=f'#{int(red.get()):02x}{int(green.get()):02x}{int(blue.get()):02x}')


red = Spinbox(okno, from_=0, to=255,state="readonly")
red.pack()

green = Spinbox(okno, from_=0, to=255,state="readonly")
green.pack()

blue = Spinbox(okno, from_=0, to=255,state="readonly")
blue.pack()

button = Button(okno, text="Farba",command=farba)
button.pack()

okno.mainloop()