import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Uloha1 {
    public static void main(String[] args) {
        try {
            FileReader fin = new FileReader("pali.txt");
            BufferedReader reader = new BufferedReader(fin);

            String row;
            int pali = 0;
            while ((row = reader.readLine()) != null) {
                if (jePalindrom(row)) {
                    pali++;
                }
            }

            System.out.println(pali);
            reader.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private static boolean jePalindrom(String s) {
        s = s.replaceAll(" ", "");
        String rev = new StringBuilder(s).reverse().toString();

        return s.equalsIgnoreCase(rev);
    }
}
