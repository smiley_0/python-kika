import tkinter as tk
import operator as op

a = tk.Tk()
a.geometry("400x400")

def pocitaj(a, b, o):
    l.config(text=str(o(a, b)))

e1 = tk.Entry(a)
e2 = tk.Entry(a)
l = tk.Label(a)
for i in [e1, e2, l]:
    i.pack()

neviem = {
    "sucet": op.add,
    "rozdiel": op.sub,
    "sucin": op.mul,
    "podiel": op.truediv,
}

for k, v in neviem.items():
    button = tk.Button(a, text=k, command=lambda v=v: pocitaj(int(e1.get()), int(e2.get()), v))
    button.pack()


a.mainloop()
