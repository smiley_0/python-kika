REPLACE = ['universe', 'space']
KITCHEN = 'kitchen'

def replace(haystack, needle):
    haystack = haystack.replace(needle.lower(), KITCHEN.lower())    # iba male
    haystack = haystack.replace(needle.upper(), KITCHEN.upper())    # iba velke
    haystack = haystack.replace(needle.title(), KITCHEN.title())    # zacina velkym

    return haystack


with open('01_guide.txt', 'r') as fin, open('out.txt', 'w') as fout:
    answer_count = 0
    nonanswer_sum = 0

    for line in fin:
        # Replace to kitchen
        l = line
        for word in REPLACE:
            l = replace(l, word)
        fout.write(l)

        # Count statistics
        for word in line.split():
            if not word.isdecimal():
                continue
            if word == '42':
                answer_count += 1
            else:
                nonanswer_sum += int(word)

    # Stats
    print(f'Answers:\t{answer_count}')
    print(f'Non-answers:\t{nonanswer_sum}')
    print(f'NA mod 42:\t{"YES" if nonanswer_sum % 42 == 0 else "NO"}')
