import sys

with open(sys.argv[1], 'r') as f:
    lower = 0
    upper = 0
    digit = 0
    newline = 0
    space = 0

    while True:
        c = f.read(1)
        if not c:
            break

        if c.islower():
            lower += 1
        elif c.isupper():
            upper += 1
        elif c.isdigit():
            digit += 1
        elif c.isspace():
            space += 1
            if c == '\n':
                newline += 1

    print(f'lower\t = {lower}')
    print(f'upper\t = {upper}')
    print(f'digit\t = {digit}')
    print(f'newline\t = {newline}')
    print(f'space\t = {space}')
