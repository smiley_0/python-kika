import sys

REPLACE = ['universe', 'space']
KITCHEN = 'kitchen'

def replace(haystack, needle):
    haystack = haystack.replace(needle.lower(), KITCHEN.lower())
    haystack = haystack.replace(needle.upper(), KITCHEN.upper())
    haystack = haystack.replace(needle.title(), KITCHEN.title())

    # Just in case
    if haystack.lower().find(needle.lower()) != -1:
        print('[WARNING] This is weird:\n\t', end='')
        print(haystack)

    return haystack


with open(sys.argv[1], 'r') as fin, open(sys.argv[2], 'w') as fout:
    answer_count = 0
    nonanswer_sum = 0

    for line in fin:
        # Replace to kitchen
        l = line
        for word in REPLACE:
            l = replace(l, word)
        fout.write(l)

        # Count statistics
        for word in line.split():
            if not word.isdecimal():
                continue
            if word == '42':
                answer_count += 1
            else:
                nonanswer_sum += int(word)

    # Stats
    print(f'Answers:\t{answer_count}')
    print(f'Non-answers:\t{nonanswer_sum}')
    print(f'NA mod 42:\t{"YES" if nonanswer_sum % 42 == 0 else "NO"}')
