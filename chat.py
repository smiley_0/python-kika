#!/usr/bin/env python3

from random import choice, randint
import re

responses = ['no jo', 'a hentak']
emoji = ['😂', '😄', '😎']

noatak = re.compile(r'no a ta+k.*')

def unknown():
    r = 'h' + ''.join('m'*randint(1,5))
    if randint(0, 1) == 0:
        r += ' okej'

    return r

def gen_emoji():
    x = randint(1,2)
    strr = ''
    for i in range(x):
        c = choice(emoji)
        strr += c + ' '
        if randint(0, 1) == 0:
            strr += c + ' '
    return strr

def message(msg=''):
    if len(msg) > 0:
        msg += ' '
    print(">> %s%s" % (msg, gen_emoji()))

def main():
    while True:
        print('>  ', end='')
        msg = input().lower().strip()

        if noatak.match(msg) is not None:
            message(choice(responses))
        else:
            if randint(0, 1) == 0:
                message(unknown())
            else:
                message()

if __name__ == '__main__':
    main()
