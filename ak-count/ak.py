#!/usr/bin/env python3

with open('prim-8.0-sane-10k.txt', 'r') as f, open('out.txt', 'w') as o:
    for line in f:
        w, c = line.strip().split()
        c = int(c)
        if w.endswith('ak') or w.endswith('ák'):
            print(w, '\t', c)
            o.write(f'{w}\n')
