public class Test {
    public static void main(String[] args) {
        System.out.println("Hello world");

        int nazov = 42;
        String test = "Java je super (nie)";

        System.out.println(test + " " + nazov);

        Lietadlo boeing = new Lietadlo(4, "N42813", 242);
        Lietadlo airbus = new Lietadlo(4, "N47477", 300);
        Lietadlo divne = new Lietadlo(3);

        boeing.povedzCoSi();
        airbus.povedzCoSi();
        divne.povedzCoSi();

        if (boeing.pocetMotorov < 4) {
            System.out.println("Je to maly boeing.");
        } else {
            System.out.println("Je to velky BOEING!");
        }

        for (int i = 0; i < 10; i++) {
            System.out.print(i + " lietadiel na letisku");

            if (i < 9) {
                System.out.println(", cakame na dalsie...");
            } else {
                System.out.println(".");
            }
        }

        String blbost = "Ine";

        switch (blbost) {
            case "Slovo":
                System.out.println("asdasf");
                break;
            case "Ine":
                System.out.println("blbooosot");
                break;
            default:
                System.out.println("niecoo");
                break;
        }
    }
}
