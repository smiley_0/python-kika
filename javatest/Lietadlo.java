import java.util.Random;

public class Lietadlo {
    public int pocetMotorov;
    public String registracneCislo;
    public int pocetOkienok;

    public Lietadlo() {
    }

    public Lietadlo(int pocetMotorov, String registracneCislo,  int pocetOkienok) {
        this.pocetMotorov = pocetMotorov;
        this.registracneCislo = registracneCislo;
        this.pocetOkienok = pocetOkienok;
    }

    public Lietadlo(int pocetMotorov) {
        Random rand = new Random();

        this.pocetMotorov = pocetMotorov;
        this.pocetOkienok = -1;
        this.registracneCislo = "N" + rand.nextInt(10000);
    }

    public void povedzCoSi() {
        System.out.println("Lietadlo " + this.registracneCislo + " ma "
            + this.pocetMotorov + " motorov, "
            + this.pocetOkienok + " okienok.");
    }
}
