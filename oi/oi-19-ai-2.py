import sys
sys.setrecursionlimit(4200000)

x = int(input())
q = int(input())
hladane = [int(i) for i in input().split()]

bankovky = [50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, x]
bankovky.sort(reverse=True)

rr = max(hladane)

table = [0 for i in range(rr + 1)]
table[0] = 0
for i in range(1, rr + 1):
    table[i] = sys.maxsize

for i in range(1, rr + 1):
    for j in range(len(bankovky)):
        if (bankovky[j] <= i):
            sub_res = table[i - bankovky[j]]
            if (sub_res != sys.maxsize and
                sub_res + 1 < table[i]):
                table[i] = sub_res + 1


for i in hladane:
    print(table[i])
