import random

n = 10000
d = 5

steps = [random.randint(1, d) for _ in range(n)]

print(n, d)
print(' '.join(map(str, steps)))
