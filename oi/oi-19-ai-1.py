import sys
sys.setrecursionlimit(1000042)

n, d = [int(i) for i in input().split()]
vysky = [int(i) for i in input().split()]
prefix = [0]
navstivene = [0 for i in range(n+1)]

for i in range(len(vysky)):
    prefix.append(prefix[i] + vysky[i])


#@profile
def dfs(vrchol):
    if vrchol == n:
        return 1
    if navstivene[vrchol] != 0:
        return navstivene[vrchol]

    s = 0
    #for i, dist in enumerate(prefix[vrchol+1:], start=vrchol+1):
    for i in range(vrchol+1, n+1):
        dist = prefix[i]
        if dist - prefix[vrchol] <= d:
            res = dfs(i)
            s = (s + res)%(10**9+7)
        else:
            break
    navstivene[vrchol] = s
    return s

print(dfs(0))
