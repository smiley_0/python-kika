import sys
sys.setrecursionlimit(4200000)

x = int(input())
q = int(input())
hladane = [int(i) for i in input().split()]

bankovky = [50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, x]
bankovky.sort(reverse=True)
nieco = {}

@profile
def zaplat(kolko, pouzite=0):
    #print('zaplat' , kolko, pouzite)
    if (kolko, pouzite) in nieco:
     #   print('aaa')
        return nieco[(kolko, pouzite)]

    if kolko == 0:
      #  print('zaplat' , kolko, pouzite, '-', 'ok done')
        return pouzite

    najmenej = 2*(10**9)
    tried = 0
    for b in bankovky:
        if kolko - b >= 0:
            if tried > 2:
                break
            tried += 1
            najmenej = min(najmenej, zaplat(kolko - b, pouzite + 1))

#    print('zaplat' , kolko, pouzite, '-', 'najlepsie zaplatim', najmenej, 'bankovky')
    nieco[(kolko, pouzite)] = najmenej

    return najmenej

for i in hladane:
    print(zaplat(i))
