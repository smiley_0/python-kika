import tkinter as tk
from math import *
from datetime import datetime

w, h = 800, 600
x0, y0 = w//2, h//2

a = tk.Canvas(width=w, height=h, bg='black')
a.pack()


def nieco_na_suradnice(angle, dist, posun_x, posun_y):
    return dist*cos(radians(angle))+posun_x, dist*sin(radians(angle))+posun_y


def ciary(angle, d_angle, l, n):
    for i in range(n):
        a.create_line(
            x0+l*cos(radians(angle)), y0+l*sin(radians(angle)),
            x0+200*cos(radians(angle)), y0+200*sin(radians(angle)),
            fill='white',
            width=3,
        )

        angle -= d_angle


a.create_oval(
    x0-200, y0-200,
    x0+200, y0+200,
    fill='grey47',
    outline='white',
    width=3,
)

ciary(-90, 30, 180, 12)
ciary(-90, 6, 190, 60)

sekundova_rucicka = a.create_line(x0, y0, x0, y0-180)
minutova_rucicka = a.create_line(x0, y0, x0, y0-160, width=2)
hodinova_rucicka = a.create_line(x0, y0, x0, y0-140, width=3, fill='red')
sec_a = -90


def vec():
    sekundy = datetime.now().second
    minuty = datetime.now().minute
    hodiny = datetime.now().hour

    if hodiny > 12: hodiny -= 12

    a.coords(
        sekundova_rucicka,
        x0, y0,
        *nieco_na_suradnice(6*sekundy-90, 180, x0, y0)
        )

    a.coords(
        minutova_rucicka,
        x0, y0,
        *nieco_na_suradnice(6*minuty-90, 160, x0, y0)
    )

    a.coords(
        hodinova_rucicka,
        x0, y0,
        *nieco_na_suradnice(30*hodiny+0.5*minuty-90, 140, x0, y0)
    )

    a.after(1, vec)

a.after(1, vec)

a.create_oval(x0-3, y0-3, x0+3, y0+3, fill='white', outline='white')

a.mainloop()