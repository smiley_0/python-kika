import tkinter as tk
from math import *
import random as rr

w, h = 700, 700
x0, y0 = w//2, h//2

a = tk.Canvas(width=w, height=h, bg='black')
a.pack()

def nieco_na_suradnice(phi, dist, posun_x, posun_y):
    return dist*cos(radians(phi))+posun_x, dist*sin(radians(phi))+posun_y


a.create_oval(
    x0-25, y0-25,
    x0+25, y0+25,
    fill='yellow'
)

pole_planet = [
    # dist, phi, 10T, ref, r, color
    [40,  -90, 2.41,   None, 3,   'gray42'],
    [60,  -90, 6.15,   None, 5,   'orange3'],
    [75,  -90, 10,     None, 5,   'DeepSkyBlue4'],
    [90,  -90, 18.8,   None, 4,   'chocolate'],
    [140, -90, 118.6,  None, 10,  'sandy brown'],
    [190, -90, 294.6,  None, 9.5, 'burlywood2'],
    [250, -90, 840.1,  None, 7,   'cyan3'],
    [330, -90, 1647.9, None, 7,   'cornflower blue'],
]

for i, p in enumerate(pole_planet):
    d = pole_planet[i][0]
    a.create_oval( x0+d, y0+d, x0-d, y0-d, outline='gray12')
    nieco = a.create_oval(0, 0, 0, 0, fill=pole_planet[i][5])
    pole_planet[i][3] = nieco

def vec():
    for i, p in enumerate(pole_planet):
        pole_planet[i][1] -= 2*pi/pole_planet[i][2]
        dist, phi, T, ref, r, color = pole_planet[i]
        x, y = nieco_na_suradnice(phi, dist, x0, y0)
        a.coords(ref, x-r, y-r, x+r, y+r)

        if phi >= 360: phi -= 360

    a.after(10, vec)


a.after(10, vec)
a.mainloop()

