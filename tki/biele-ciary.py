import tkinter as tk
a = tk.Canvas(width=600, height=600, bg='black')
a.pack()

x, y = 0, 0

for i in range(30):
    a.create_line(
        x+i*20, y,
        x+600, y+i*20,
        fill='white',
    )

    a.after(50)
    a.update()

for i in range(30):
    a.create_line(
        x+600, y+i*20,
        x+600-i*20, y+600,
        fill='white',
    )

    a.after(50)
    a.update()

for i in range(30):
    a.create_line(
        x+600-i*20, y+600,
        x, y+600-i*20,
        fill='white',
    )

    a.after(50)
    a.update()

for i in range(30):
    a.create_line(
        x, y+600-i*20,
        x+i*20, y,
        fill='white',
    )

    a.after(50)
    a.update()

a.mainloop()