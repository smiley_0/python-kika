import tkinter as tk
a = tk.Canvas(width=800, height=600, bg='black')

def click_action(event):
    print('click', event)

    item = a.find_withtag(tk.CURRENT)
    print(item)

    a.itemconfig(tk.CURRENT, fill="blue")

def trojuholnik(x, y, w, h):
    it = a.create_polygon(
        (x, y),
        (x+w, y),
        (x+w//2, y-h),
        fill='red'
    )

    a.tag_bind(it, '<Button-1>', click_action)


def nieco(x, y, w, h, n):
    if n <= 0:
        trojuholnik(x, y, w, h)
    else:
        nieco(x, y, w//2, h//2, n-1)
        nieco(x+w//2, y, w//2, h//2, n-1)
        nieco(x+w//4, y-h//2, w//2, h//2, n-1)


nieco(100, 550, 600, 500, 3)

a.pack()
tk.mainloop()
