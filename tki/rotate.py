import tkinter as tk
import math

def get_coords(distance, angle, center=(0, 0)):
    """Calculate coordinates of object on a circle"""
    cx, cy = center
    return cx + math.cos(math.radians(angle)) * distance, cy + math.sin(math.radians(angle)) * distance

def gc(center, radius):
    """Get x1, y1, x2, y2 for circle with specified center and radius"""
    cx, cy = center
    return cx-radius, cy-radius, cx+radius, cy+radius

# Canvas
w, h = 800, 600
a = tk.Canvas(width=w, height=h, bg='black')

# Calculate and visualize center
cx, cy = w//2, h//2
center = a.create_oval(*gc((cx, cy), 20), fill='yellow')

# Planet parameters
dist = 200
r = 10
angle = 0

# Calculate initial planet coordinates
ox, oy = get_coords(dist, angle, (cx, cy))

# Draw orbit and planet
orbit = a.create_oval(*gc((cx, cy), dist), outline='gray')
obj = a.create_oval(*gc((ox, oy), r), fill='blue')

# Animate
def animation():
    global angle, dist

    # Update angle and distance from center (because we can)
    angle += 1
    dist += 0.8 * math.sin(math.radians(angle))

    # Calculate new coordinates and move planet there
    a.coords(obj, *gc(get_coords(dist, angle, (cx, cy)), r))

    # Update orbit
    a.coords(orbit, *gc((cx, cy), dist))

    # Repeat after 15ms
    a.after(15, animation)

a.after(15, animation)
a.pack()
tk.mainloop()
