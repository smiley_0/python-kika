import tkinter as tk
from math import *
a = tk.Canvas(width=600, height=600, bg='black')
a.pack()

x0, y0 = 300, 300

a.create_oval(
    x0-175, y0-175,
    x0+175, y0+175,
    fill='red'
)

a.create_oval(
    x0-173, y0-173,
    x0+173, y0+173,
    fill='black'
)

a.create_oval(
    x0-40, y0-40,
    x0+40, y0+40,
    fill='yellow'
)

def kruh_(x, y):
    return a.create_oval(
        x+10, y+10,
        x-10, y-10,
        fill='blue'
)

kruh = kruh_(300, 125)
x, y, f = 300, 125, 90

# z nejakeho dovodu to ide po elipse a nie po kruznici
def vec():
    global f, x, y
    f -= 1
    nx, ny = 175*cos(radians(f))+300, 175*sin(radians(f))-50
    dx, dy = x - nx, y - ny
    x, y = nx, ny
    a.move(kruh, dx, dy)
    a.after(10, vec)

a.after(10, vec)
a.mainloop()
