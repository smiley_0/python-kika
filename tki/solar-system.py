import tkinter as tk
from math import *
import random as rr

w, h = 700, 700
x0, y0 = w//2, h//2

a = tk.Canvas(width=w, height=h, bg='black')
a.pack()

def nieco_na_suradnice(phi, dist, posun_x, posun_y):
    return dist*cos(radians(phi))+posun_x, dist*sin(radians(phi))+posun_y


a.create_oval(
    x0-20, y0-20,
    x0+20, y0+20,
    fill='yellow'
)

pole_planet = [
    # dist, phi, 10T, ref, r, color
    [60, -90, 2.41, None, 10, 'gray42'],
    [100, -90, 6.15, None, 10, 'DarkOliveGreen4'],
    [140, -90, 10, None, 10, 'DeepSkyBlue'],
    [180, -90, 18.8, None, 10, 'chocolate'],
    [220, -90, 118.6, None, 10, 'sandy brown'],
    [260, -90, 294.6, None, 10, 'burlywood2'],
    [280, -90, 840.1, None, 10, 'cyan3'],
    [320, -90, 1647.9, None, 10, 'cornflower blue'],
]

for i, p in enumerate(pole_planet):
    d = pole_planet[i][0]
    a.create_oval( x0+d, y0+d, x0-d, y0-d, outline='grey')
    nieco = a.create_oval(0, 0, 0, 0, fill=pole_planet[i][5])
    pole_planet[i][3] = nieco

def vec():
    for i, p in enumerate(pole_planet):
        pole_planet[i][1] -= 2*pi/pole_planet[i][2]
        dist, phi, T, ref, r, *_ = pole_planet[i]
        x, y = nieco_na_suradnice(phi, dist, x0, y0)
        a.coords(ref, x-r, y-r, x+r, y+r)

    a.after(10, vec)


a.after(10, vec)
a.mainloop()

