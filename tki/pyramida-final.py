# nezmazanim tohto komentara sa priznavam
# k tomu, ze som nedocitala zadanie

import tkinter as tk
from random import choice as ch

a = tk.Canvas(width=800, height=600, bg="black")

s, d, n = int(input()), int(input()), int(input()) #sirka, vyska, pocet
x, y = (800-n*s)//2, 600

def pyramida():
    global s, n, d, x, y
    if n > 0:
        for i in range(n):
            a.create_rectangle(
                x+i*s, y-d,
                x+(i+1)*s, y,
                fill=ch(('pink', 'red')),
            )

        x += s//2
        y -= d
        n -= 1
        pyramida()


pyramida()


a.pack()
tk.mainloop()
