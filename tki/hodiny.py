import tkinter as tk
from math import *
from datetime import datetime

cas = datetime.now()
hodina, minuta, sekunda = cas.hour, cas.minute, cas.second

w, h = 800, 600
x0, y0 = w//2, h//2

a = tk.Canvas(width=w, height=h, bg='black')
a.pack()


def nieco_na_suradnice(angle, dist, posun_x, posun_y):
    return dist*cos(radians(angle))+posun_x, dist*sin(radians(angle))+posun_y


def ciary(angle, d_angle, l, n):
    for i in range(n):
        a.create_line(
            x0+l*cos(radians(angle)), y0+l*sin(radians(angle)),
            x0+200*cos(radians(angle)), y0+200*sin(radians(angle)),
            fill='white',
            width=3,
        )

        angle -= d_angle


a.create_oval(
    x0-200, y0-200,
    x0+200, y0+200,
    fill='grey47',
    outline='white',
    width=3,
)

ciary(-90, 30, 180, 12)
ciary(-90, 6, 190, 60)

sekundova_rucicka = a.create_line(x0, y0, x0, y0-180)
minutova_rucicka = a.create_line(x0, y0, x0, y0-160, width=2)
sec_a = -90


def sekundova_vec():
    global sec_a
    sec_a += 6
    a.coords(sekundova_rucicka, x0, y0, *nieco_na_suradnice(sec_a, 180, x0, y0))
    a.after(1000, sekundova_vec)


def minutova_vec():
    global sec_a
    sec_a += 6
    a.coords(minutova_rucicka, x0, y0, *nieco_na_suradnice(sec_a, 160, x0, y0))
    a.after(600, minutova_vec)

a.after(1000, sekundova_vec)
a.after(600, minutova_vec)

a.create_oval(x0-3, y0-3, x0+3, y0+3, fill='white', outline='white')

a.mainloop()