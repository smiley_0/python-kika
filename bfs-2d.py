from collections import deque

w, h = [int(i) for i in input().split()]
bludisko = [[i for i in input()] for i in range(h)]
dist = [[-1 for i in range(w)]for i in range(h)]

for r in range(h):
    for s in range(w):
        if bludisko[r][s] == 'S':
            S = (r, s)
        elif bludisko[r][s] == 'F':
            F = (r, s)

q = deque([S])
nieco = [(0, 1), (0, -1), (1, 0), (-1, 0)]
dist[S[0]][S[1]] = 0

while len(q) != 0:
    y, x = q.pop()

    for a, b in nieco:
        yy, xx = y + a, x + b

        if xx >= w or xx < 0:
            continue

        if yy >= h or yy < 0:
            continue

        if (bludisko[yy][xx] == '.' or bludisko[yy][xx] == 'F') and (dist[yy][xx] > dist[y][x]+1 or dist[yy][xx] == -1):
            q.append((yy, xx))
            dist[yy][xx] = dist[y][x] + 1

print(dist[F[0]][F[1]])
